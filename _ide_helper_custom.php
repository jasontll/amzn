<?php
namespace Illuminate\Support {
    exit("This file should not be included, only analyzed by your IDE");
    /**
     * @method Fluent first()
     * @method Fluent after($column)
     * @method Fluent change()
     * @method Fluent nullable()
     * @method Fluent unsigned()
     * @method Fluent unique()
     * @method Fluent index()
     * @method Fluent primary()
     * @method Fluent default($value)
     * @method Fluent onUpdate($value)
     * @method Fluent onDelete($value)
     * @method Fluent references($value)
     * @method Fluent on($value)
     */
    class Fluent {
    }
}

