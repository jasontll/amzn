var elixir = require('laravel-elixir');
var vueify = require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.browserify('vue-components.js');
    
    mix.sass([
        'base.scss'
    ], 'public/css/base.css');
    
    mix.sass([
        'app.scss'
    ]);
    
    mix.styles([
        'css/base.css',
        'vendor/global/plugins/simple-line-icons/simple-line-icons.css',
        'vendor/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
        'vendor/global/plugins/uniform/css/uniform.default.css',
        'vendor/global/plugins/select2/css/select2.css',
        'vendor/global/plugins/select2/css/select2-bootstrap.min.css',
        'vendor/global/css/components-md.css',
        'vendor/global/css/plugins-md.css',
        'vendor/global/plugins/bootstrap-toastr/toastr.css',
        'vendor/global/plugins/medium-editor/css/medium-editor.css',
        'vendor/layout/css/layout.css',
        'vendor/layout/css/themes/blue.css',
        'css/app.css'
    ], 'public/css/compiled.css', 'public');
    
    mix.scripts([
        './public/vendor/global/plugins/js.cookie.min.js',
        './public/vendor/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        './public/vendor/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        './public/vendor/global/plugins/jquery.blockui.min.js',
        './public/vendor/global/plugins/uniform/jquery.uniform.min.js',
        './public/vendor/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        './public/vendor/global/plugins/select2/js/select2.full.js',
        './public/vendor/global/plugins/bootstrap-toastr/toastr.js',
        './public/vendor/global/plugins/medium-editor/js/medium-editor.js',
        './public/vendor/global/js/app.js',
        './public/vendor/layout/js/layout.js',
        //'vendor/layout/js/theme-setting.js',
        './public/vendor/layout/js/quick-sidebar.js',
        './public/js/vue-components.js',
        'app.js'
    ], 'public/js/app.js');

    mix.version(['public/css/compiled.css', 'public/js/app.js']);

});