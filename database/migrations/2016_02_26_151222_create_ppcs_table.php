<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpcsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppcs', function (Blueprint $table)
        {
            $table->increments('id');
            $table->timestamp('StartDate');
            $table->timestamp('EndDate');
            $table->string('MerchantName');
            $table->string('SKU')->index();
            $table->integer('Clicks');
            $table->integer('Impressions');
            $table->decimal('CTR', 8, 5);
            $table->string('Currency');
            $table->decimal('TotalSpend');
            $table->decimal('AvgCPC');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ppcs');
    }
}
