<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_queues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ReportRequestId')->unique()->index();
            $table->string('ReportId')->nullable();
            $table->string('ReportType');
            $table->string('Status')->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_queues');
    }
}
