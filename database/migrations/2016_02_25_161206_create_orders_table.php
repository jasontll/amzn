<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('AmazonOrderId')->index();
            $table->timestamp('PurchaseDate')->index();
            $table->timestamp('LastUpdatedDate')->index();
            $table->string('OrderStatus')->index();
            $table->string('SKU')->index();
            $table->string('ASIN')->index();
            $table->string('FulfillmentChannel');
            $table->string('SalesChannel');
            $table->string('ShipServiceLevel');
            $table->string('ShipCity');
            $table->string('ShipState');
            $table->string('ShipPostalCode');
            $table->string('ShipCountry');
            $table->integer('Quantity');
            $table->string('Currency');
            $table->decimal('ItemPrice');
            $table->decimal('ItemTax');
            $table->decimal('ShippingPrice');
            $table->decimal('ShippingTax');
            $table->decimal('GiftWrapPrice');
            $table->decimal('GiftWrapTax');
            $table->decimal('ItemPromotionDiscount');
            $table->decimal('ShipPromotionDiscount');
            $table->string('PromotionIds')->index();
            $table->timestamps();
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
