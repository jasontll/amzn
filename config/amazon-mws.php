<?php

return [
    'store'              => [
        'GoFurball' => [
            'merchantId'       => env('MWS_SELLER_ID'),
            'marketplaceId'    => env('MWS_MARKETPLACE_ID'),
            'keyId'            => env('AWS_ACCESS_KEY_ID'),
            'secretKey'        => env('MWS_SECRET_KEY'),
            'amazonServiceUrl' => 'https://mws.amazonservices.com/',
        ]
    ],

    // Default service URL
    'AMAZON_SERVICE_URL' => 'https://mws.amazonservices.com/',

    'muteLog' => false
];
