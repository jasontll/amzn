<?php

return [

    /*
    |—————————————————————————————————————
    | Default Errors
    |—————————————————————————————————————
    */

    'bad_request' => [
        'message' => 'Your request had an error. Please try again.',
        'detail'  => 'The server cannot or will not process the request due to something that is perceived to be a client error.'

    ],

    'forbidden' => [
        'message' => 'Your request was valid, but you are not authorised to perform that action.',
        'detail'  => 'The request was a valid request, but the server is refusing to respond to it.'

    ],

    'not_found' => [
        'message' => 'The resource you were looking for was not found.',
        'detail'  => 'The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.'

    ],

    'precondition_failed' => [
        'message' => 'Your request did not satisfy the required preconditions.',
        'detail'  => 'The server does not meet one of the preconditions that the requester put on the request.'

    ],

    'internal_server_error' => [
    'message' => 'There is an internal error, please contact tech support.',
    'detail'  => 'The server is experiencing internal error upon request, this should be reported to the tech support.'

]

];