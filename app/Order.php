<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Order extends Model {

    protected $fillable = [
        'AmazonOrderId',
        'PurchaseDate',
        'LastUpdatedDate',
        'OrderStatus',
        'SKU',
        'ASIN',
        'FulfillmentChannel',
        'SalesChannel',
        'ShipServiceLevel',
        'ShipCity',
        'ShipState',
        'ShipPostalCode',
        'ShipCountry',
        'Quantity',
        'Currency',
        'ItemPrice',
        'ItemTax',
        'ShippingPrice',
        'ShippingTax',
        'GiftWrapPrice',
        'GiftWrapTax',
        'ItemPromotionDiscount',
        'ShipPromotionDiscount',
        'PromotionIds'
    ];

    protected $dates = [
        'PurchaseDate'
    ];

    public function scopeYtd($query)
    {
        return $query->where(['SalesChannel' => 'Amazon.com'])
                     ->where('OrderStatus', '!=', 'Cancelled')
                     ->where('PurchaseDate', '>=', Carbon::today('America/Los_Angeles')->startOfYear());
    }

    public static function createIfFirst(array $attributes, array $values)
    {
        if (is_null($instance = Order::where($attributes)->first()))
        {
            $instance = Order::create($values);
        }

        return $instance;
    }

}
