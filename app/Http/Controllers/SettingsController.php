<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

class SettingsController extends Controller
{

    public function index()
    {
        return view('settings.index');
    }

    /**
     * @param $id
     */
    public function userAccount($id)
    {
        $user = User::findOrFail($id);

        return view('settings.user-account', compact('user'));
    }

}

