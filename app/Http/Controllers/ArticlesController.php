<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\ArticleRequest;
use App\Tag;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;


class ArticlesController extends Controller {

    /**
     * ArticlesController constructor.
     * Create a new articles controller instance.
     * Require authentication for create and edit
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display all articles
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = Article::latest('published_at')->published()->get();

        foreach ($articles as $article)
        {
            $tags = $article->tags->toArray();
            array_merge($article->toArray(), $tags);
        }

        return view('articles.index', compact('articles'));

    }

    /**
     * Display specific article
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Article $article)
    {
        return view('articles.show', compact('article'));
    }

    /**
     * Send user to edit article page
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Article $article)
    {
        $tags = Tag::lists('name', 'id');

        return view('articles.edit', compact('article', 'tags'));
    }

    /**
     * Return to create article page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $tags = Tag::lists('name', 'id');
        return view('articles.create', compact('tags'));
    }

    /**
     * Create a new article
     *
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleRequest $request)
    {
        $this->createArticle($request);

        session()->flash('success', 'Article successfully created.');
        return redirect('articles');
    }

    /**
     * Update Article
     *
     * @param Article        $article
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Article $article, ArticleRequest $request)
    {
        $this->updateArticle($article, $request);
        session()->flash('success', 'Article successfully updated.');
        return redirect('articles');
    }

    /**
     * Sync up the list of tags in the database
     *
     * @param Article        $article
     * @param ArticleRequest $request
     */
    private function syncTags(Article $article, Array $tags)
    {
        $article->tags()->sync($tags);
    }

    /**
     * Save a new article
     *
     * @param ArticleRequest $request
     * @return mixed
     */
    private function createArticle(ArticleRequest $request)
    {
        $article = Auth::user()->articles()->create($request->all());

        $this->syncTags($article, $request->input('tag_list'));

        return $article;
    }

    /**
     * @param Article        $article
     * @param ArticleRequest $request
     */
    private function updateArticle(Article $article, ArticleRequest $request)
    {
        $article->update($request->all());
        $this->syncTags($article, $request->input('tag_list'));

        return $article;
    }
}
