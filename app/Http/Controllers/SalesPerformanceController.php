<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\SalesPerformance;
use App\Transformers\LastWeekPerformanceTransformer;
use App\Transformers\SalesPerformanceTransformer;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

/**
 * Class SalesPerformanceController
 *
 * @package App\Http\Controllers
 */
class SalesPerformanceController extends ApiController {

    protected $salesPerformanceTransformer, $lastWeekSalesPerformanceTransformer;

    public function __construct(SalesPerformanceTransformer $salesPerformanceTransformer,
                                LastWeekPerformanceTransformer $lastWeekPerformanceTransformer)
    {
        $this->salesPerformanceTransformer         = $salesPerformanceTransformer;
        $this->lastWeekSalesPerformanceTransformer = $lastWeekPerformanceTransformer;
    }

    public function index()
    {
        $limit = Input::get('limit') ?: 15;

        $startDate = (Input::get('startDate') ? Carbon::parse(Input::get('startDate'))->toDateString() : false);
        $endDate   = (Input::get('endDate') ? Carbon::parse(Input::get('endDate'))->toDateString() : Carbon::today());

        $data = $startDate ? $this->salesPerformanceByDate($startDate, $endDate) : $this->salesPerformance();

        return $data->count() == 0 ? $this->respondNotFound('No performance data found.') : $this->respond([
            'data' => $this->salesPerformanceTransformer->transformCollections($data->all())
        ]);
    }

    public function show($productType)
    {
        $limit = Input::get('limit') ?: 15;

        $data = $this->salesPerformanceByProductType($productType, $limit);

        return $data->count() == 0 ? $this->respondNotFound('No performance data found.') : $this->respondWithPagination($data, [
            'data' => $this->salesPerformanceTransformer->transformCollections($data->all())
        ]);
    }

    public function weekly()
    {

        $productType = Input::get('type');

        $data = $this->getTwoWeeksData($productType);

        dd($data);

//        return $data->count() == 0 ? $this->respondNotFound(sprintf('No data for date from %s - %s', $begin, $end)) : $this->respond([
//            'data' => $this->salesPerformanceTransformer->transformCollections($data->all())
//        ]);

    }

    /**
     * @return mixed
     */
    private function salesPerformance()
    {
        $adsPerformance = SalesPerformance::groupBy(['date', 'product_type'])
                                          ->get([
                                              DB::raw('max(date) as date'),
                                              DB::raw('max(product_type) as product_type'),
                                              DB::raw('sum(clicks) as clicks'),
                                              DB::raw('sum(revenue) as revenue'),
                                              DB::raw('sum(spend) as spend'),
                                              DB::raw('sum(quantity) as quantity'),
                                              DB::raw('sum(impressions) as total_impressions'),
                                          ]);

        return $adsPerformance;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    private function salesPerformanceByDate($startDate, $endDate)
    {
        $adsPerformance = SalesPerformance::where([['date', '>=', $startDate], ['date', '<=', $endDate]])
                                          ->groupBy(['date'])
                                          ->get([
                                              DB::raw('max(date) as date'),
                                              DB::raw('"All" as product_type'),
                                              DB::raw('sum(clicks) as clicks'),
                                              DB::raw('sum(revenue) as revenue'),
                                              DB::raw('sum(spend) as spend'),
                                              DB::raw('sum(quantity) as quantity'),
                                              DB::raw('sum(impressions) as total_impressions'),
                                          ]);

        return $adsPerformance;
    }

    /**
     * @param $product_type
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    private function salesPerformanceByProductTypeAndDate($product_type, $startDate, $endDate)
    {
        $adsPerformance = SalesPerformance::where([['product_type', $product_type], ['date', '>=', $startDate], ['date', '<=', $endDate]])
                                          ->groupBy(['date', 'product_type'])
                                          ->get([
                                              DB::raw('max(date) as date'),
                                              DB::raw('max(product_type) as product_type'),
                                              DB::raw('sum(clicks) as clicks'),
                                              DB::raw('sum(revenue) as revenue'),
                                              DB::raw('sum(spend) as spend'),
                                              DB::raw('sum(quantity) as quantity'),
                                              DB::raw('sum(impressions) as total_impressions'),
                                          ]);

        return $adsPerformance;
    }

    /**
     * @param $productType
     * @param $limit
     * @return mixed
     */
    private function salesPerformanceByProductType($productType, $limit)
    {
        $adsPerformance = SalesPerformance::where('product_type', $productType)
                                          ->groupBy(['date', 'product_type'])
                                          ->orderBy('date', 'desc')
                                          ->paginate($limit, [
                                              DB::raw('max(date) as date'),
                                              DB::raw('max(product_type) as product_type'),
                                              DB::raw('sum(clicks) as clicks'),
                                              DB::raw('sum(revenue) as revenue'),
                                              DB::raw('sum(spend) as spend'),
                                              DB::raw('sum(quantity) as quantity'),
                                              DB::raw('sum(impressions) as total_impressions')
                                          ]);

        return $adsPerformance;
    }

    /**
     * @param $productType
     * @return array
     */
    public function getTwoWeeksData($productType)
    {
        $begin = Carbon::today()->startOfWeek();
        $end   = Carbon::today()->endOfWeek();

        $lastWeekBegin = Carbon::parse('last week')->startOfWeek();
        $lastWeekEnd   = Carbon::parse('last week')->endOfWeek();

        if ($productType)
        {
            $thisWeekData = $this->salesPerformanceByProductTypeAndDate($productType, $begin, $end);
            $lastWeekData = $this->salesPerformanceByProductTypeAndDate($productType, $lastWeekBegin, $lastWeekEnd);
        } else
        {
            $thisWeekData = $this->salesPerformanceByDate($begin, $end);
            $lastWeekData = $this->salesPerformanceByDate($lastWeekBegin, $lastWeekEnd);
        }

        $thisWeekData = $this->salesPerformanceTransformer->transformCollections($thisWeekData->all());
        $lastWeekData = $this->lastWeekSalesPerformanceTransformer->transformCollections($lastWeekData->all());

        $thisWeekData = collect($thisWeekData)->keyBy('day');
        $lastWeekData = collect($lastWeekData)->keyBy('lastWeek_day');
        $dataSet      = $this->mergeData($thisWeekData, $lastWeekData);

        return $dataSet;

    }

    /**
     * @param Collection $dataSets
     * @return array
     */
    private function mergeData($thisWeekData, $lastWeekData)
    {
        $data = [];
        foreach ($lastWeekData as $lastWeekKey => $lastWeekValues){
            foreach ($thisWeekData as $thisWeekKey => $thisWeekValues){
                if ($thisWeekKey == $lastWeekKey)
                {
                    foreach ($thisWeekValues as $key => $thisWeekValue)
                    {
                        $lastWeekValues[$key] = $thisWeekValue;
                    }
                }
                array_push($data,$lastWeekValues);
            }
        }
        return $data;
    }

}
