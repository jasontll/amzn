<?php

namespace App\Http\Controllers;

use App\Helpers\MwsHelper;
use App\Http\Requests;
use App\Mws\ProductManager;
use Response;

class DashboardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $helper;

    public function __construct(MwsHelper $helper)
    {
        $this->middleware('admin');
        $this->helper = $helper;
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

}