<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Mws\ProductManager as MwsProduct;
use App\Product;
use App\Transformers\ProductTransformer;
use Illuminate\Support\Facades\Input;

class ProductsController extends ApiController {

    /**
     * @var ProductTransformer
     */
    protected $productTransformer;
    protected $mwsProduct;

    /**
     * ProductController constructor.
     *
     * @param ProductTransformer $productTransformer
     */
    public function __construct(ProductTransformer $productTransformer, MwsProduct $mwsProduct)
    {
        $this->productTransformer = $productTransformer;
        $this->mwsProduct         = $mwsProduct;
    }

    public function index()
    {
        return view('products.index');
    }

    public function all()
    {
        $limit = Input::get('limit') ?: 25;

        $products = Product::paginate($limit);

        if ($products->total() == 0)
        {
            return $this->respondNotFound('No products found.');
        }

        return $this->respondWithPagination($products, [
            'data' => $this->productTransformer->transformCollections($products->all())
        ]);
    }

    public function show($id)
    {
        $product = Product::where('sku', $id)
                          ->orWhere('asin', $id)
                          ->first();

        if (!$product)
        {
            return $this->respondNotFound('Product does not exist');
        }

        return $this->respond([
            'data' => $this->productTransformer->transform($product)
        ]);

    }

    public function import()
    {
        $products       = $this->mwsProduct->importLatest();
        $numberImported = $products->count();

        return $this->respondOk($numberImported . ' products imported');

    }

    public function update($asin)
    {
        Product::where('asin', $asin)
               ->update([
                   'product_title'  => Input::get('product_title'),
                   'internal_title' => Input::get('internal_title'),
                   'description'    => Input::get('description'),
                   'product_type'    => Input::get('product_type')
               ]);

        return $this->respondOk('Product updated.');
    }

    public function edit($asin)
    {
        $product = Product::where('asin', $asin)
                          ->first();

        return view('products.edit', compact('product'));
    }

}
