<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Invitation;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Input;
use Mail;
use Symfony\Component\HttpFoundation\Response;

class InvitationsController extends ApiController {

    protected $invitationTransformer;

    /**
     * InvitationController constructor.
     */
    public function __construct(UserTransformer $invitationTransformer)
    {
        $this->invitationTransformer = $invitationTransformer;
    }

    /**
     * @param      $email
     * @param bool $reminder
     */
    public function send($email, $reminder = false)
    {
        if (!$reminder)
        {
            Invitation::create([
                'email' => $email,
                'token' => str_random(60)
            ]);
        }

        $token = Invitation::where(['email' => $email])
                           ->firstOrFail();

        Mail::queue('emails.invitation', ['token' => $token], function ($message) use ($token)
        {
            $message->to($token->email, null)->subject('Invitation From Trendelous');
        });
    }
}