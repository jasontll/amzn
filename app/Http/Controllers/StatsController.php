<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\SalesPerformance;
use App\Transformers\StatsTransformer;

class StatsController extends ApiController {

    protected $statsTransformer, $salesData;
    /**
     * StatsController constructor.
     *
     * @param StatsTransformer $statsTransformer
     */
    public function __construct(StatsTransformer $statsTransformer)
    {
        $this->statsTransformer = $statsTransformer;
        $this->salesData;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $salesData = $this->salesData(['revenue', 'quantity']);

        return $this->respond([
            'data' => $this->statsTransformer->transform($salesData)
        ]);
    }

    /**
     * @param array $columns
     * @return \Illuminate\Support\Collection
     */
    public function salesData($columns = [])
    {
        $columns         = collect($columns);
        $this->salesData = collect([]);

        $columns->each(function ($column, $key)
        {
            $yearToDate  = SalesPerformance::yearToDate()->sum($column);
            $monthToDate = SalesPerformance::monthToDate()->sum($column);
            $weekToDate  = SalesPerformance::weekToDate()->sum($column);
            $today       = SalesPerformance::today()->sum($column);

            $this->salesData->put(sprintf('%s_ytd', $column), $yearToDate);
            $this->salesData->put(sprintf('%s_mtd', $column), $monthToDate);
            $this->salesData->put(sprintf('%s_wtd', $column), $weekToDate);
            $this->salesData->put(sprintf('%s_today', $column), $today);
        });

        return $this->salesData;
    }


}
