<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ProductTypes;
use Illuminate\Support\Facades\Input;

class ProductTypesController extends ApiController {

    public function index()
    {
        $productTypes = ProductTypes::all();

        return $this->respond([
            'data' => $productTypes
        ]);
    }

    public function create()
    {
        $productType = ProductTypes::create([
            'name' => Input::get('new_product_type')
        ]);

        return $this->respondCreated(sprintf('%s added.', $productType->name));
    }
}
