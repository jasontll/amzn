<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mail;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends ApiController {

    protected $userTransformer;

    /**
     * UsersController constructor.
     */
    public function __construct(UserTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;
    }

    public function index()
    {
        $limit = Input::get('limit') ?: 5;

        $users = User::paginate($limit);

        if ($users->total() == 0)
        {
            return $this->respondNotFound('No users found.');
        }

        return $this->respondWithPagination($users, [
            'data' => $this->userTransformer->transformCollections($users->all())
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function invite()
    {

        /** @var User $invitation */
        $invitation = User::where('email', Input::get('email'))
                          ->pending()->get();

        if ($invitation->isEmpty())
        {
            $invitation = User::create([
                'first_name'       => Input::get('first_name'),
                'last_name'        => Input::get('last_name'),
                'email'            => Input::get('email'),
                'invitation_token' => str_random(60)
            ]);

            //$this->sendEmail($invitation);

            return $this->respondOk(
                sprintf('Invitation sent to %s %s.', $invitation->first_name, $invitation->last_name)
            );
        }

        return $this->setStatusCode(Response::HTTP_FORBIDDEN)
                    ->respondWithError(
                        sprintf('%s %s already has a pending invitation.', $invitation->first_name, $invitation->last_name)
                    );
    }

    public function delete($id)
    {
        $user = User::find($id)->first();

        if ($user)
        {
            User::destroy($id);

            return $this->respondOk('User has been deleted.');
        };

        return $this->respondNotFound('User not found, nothing is deleted.');

    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'email|required'
        ]);

        User::find(Input::get('id'))
            ->update([
                'first_name' => Input::get('first_name'),
                'last_name'  => Input::get('last_name'),
                'email'      => Input::get('email')
            ]);

        return $this->respondOk('User information updated.');

    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:10'
        ]);

        User::find(Input::get('id'))
            ->update([
               'password' => bcrypt(Input::get('password'))
            ]);

        return $this->respondOk('User password updated.');
    }

    /**
     * Send invitation email
     *
     * @param $invitation
     */
    private function sendEmail($invitation)
    {
        Mail::queue('emails.invitation', ['invitation' => $invitation], function ($message) use ($invitation)
        {
            $message->to($invitation->email, $invitation->first_name)->subject('Invitation From Trendelo.us');
        });
    }

}