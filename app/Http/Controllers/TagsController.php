<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Http\Requests;


class TagsController extends Controller
{

    public function show(Tag $tag)
    {
        $articles = $tag->articles()->latest('published_at')->published()->get();

        return view('articles.index', compact('articles'));
    }
}