<?php
namespace app\Http\Composers;

use App\Article;
use App\Tag;

class SingleArticleSidebarComposer {

    public function compose($view)
    {
        $tags = Tag::all()->sortBy('name');
        $latestArticles = Article::latest('published_at')->published()->take(5)->get();

        $view->with(['tags' => $tags, 'latestArticles' => $latestArticles]);
    }
}