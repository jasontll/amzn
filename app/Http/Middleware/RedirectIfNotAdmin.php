<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if (env('APP_ENV') == 'local') {
//            return $next($request);
//        }

        $user = $request->user();
        if ($user && $user->isAdmin($user->email))
        {
            return $next($request);
        }
        return redirect('/');
    }
}
