<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Session;

class MustBeInvited {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var User $token */
        $token = $this->token($request);

        if (!$token)
        {
            return view('errors.403', ['message' => 'You must be invited to register']);
        }

        Session::flash('user', $token);

        return $next($request);
    }

    /**
     * @param $request
     */
    private function token($request)
    {
        return User::where('invitation_token', $request->token)
                   ->select('first_name', 'last_name', 'email', 'invitation_token')
                   ->first();
    }


}
