<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function ()
{
    Route::auth();
    Route::get('/', [
        'as' => 'landing', function ()
        {
            if (Auth::check())
            {
                return redirect('dashboard');
            }
            return redirect('login');
        }
    ]);

    Route::group(['prefix' => 'dashboard'], function ()
    {
        Route::get('/', 'DashboardController@index');
    });

    Route::resource('articles', 'ArticlesController');
    Route::get('tags/{tags}', 'TagsController@show');

    Route::group(['prefix' => 'products', 'middleware' => 'admin'], function ()
    {
        Route::get('/','ProductsController@index');
        Route::get('/{asin}/edit','ProductsController@edit');
    });

    Route::group(['prefix' => 'settings', 'middleware' => 'admin'], function ()
    {
        Route::get('/', 'SettingsController@index');
        Route::get('/users/{id}', 'SettingsController@userAccount');
    });

    //    Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function ()
    Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function ()
    {
        Route::get('stats', 'StatsController@index');
        Route::get('sales-performance', 'SalesPerformanceController@weekly');
        Route::get('sales-performance/{productType}', 'SalesPerformanceController@show');


        Route::post('users', 'UsersController@invite');
        Route::get('users', 'UsersController@index');
        Route::delete('users/{id}', 'UsersController@delete');
        Route::get('users/{id}', 'UsersController@show');
        Route::put('users/{id}', 'UsersController@update');
        Route::put('users/{id}/password', 'UsersController@updatePassword');

        Route::get('products', 'ProductsController@all');
        Route::get('products/import', 'ProductsController@import');
        Route::put('products/{asin}', 'ProductsController@update');

        Route::get('product-types', 'ProductTypesController@index');
        Route::post('product-types', 'ProductTypesController@create');
    });

}
);
