<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SalesPerformance extends Model {

    /**
     * Mysql view for the model
     *
     * @var string
     */
    protected $table = 'view_sales_spending';


    /**
     * @param $query \Eloquent
     * @return mixed
     */
    public function scopeYearToDate($query)
    {
        return $query->where('date', '>=', Carbon::today('America/Los_Angeles')->startOfYear());
    }

    /**
     * @param $query \Eloquent
     * @return mixed
     */
    public function scopeMonthToDate($query)
    {
        return $query->where('date', '>=', Carbon::today('America/Los_Angeles')->startOfMonth());
    }

    /**
     * @param $query \Eloquent
     * @return mixed
     */
    public function scopeWeekToDate($query)
    {
        return $query->where('date', '>=', Carbon::today('America/Los_Angeles')->startOfWeek());
    }

    /**
     * @param $query \Eloquent
     * @return mixed
     */
    public function scopeToday($query)
    {
        return $query->where('date', '>=', Carbon::today('America/Los_Angeles'));
    }

}
