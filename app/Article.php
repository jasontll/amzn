<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model {

    protected $fillable = [
        'title',
        'body',
        'slug',
        'published_at'
    ];

    protected $dates = [
        'published_at'
    ];

    /**
     * Convert published date to Carbon's type
     *
     * @param $date
     */
    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = (Carbon::parse($date)->isToday()? Carbon::now() : Carbon::parse($date));
    }

    public function getPublishedAtAttribute($date)
    {
        return new Carbon($date);
    }

    /**
     * Get published articles only
     *
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now());
    }

    /**
     * Get unpublished articles only
     *
     * @param $query
     * @return mixed
     */
    public function scopeUnPublished($query)
    {
        return $query->where('published_at', '>=', Carbon::now());
    }

    /**
     * An article is owned by a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the tags associated with the article
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    public function getTagListAttribute()
    {
        return $this->tags->lists('id')->all();
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($article)
        {
            $article['slug'] = str_slug($article['title']);

            $latestSlug =
                static::whereRaw("slug RLIKE '^{$article['slug']}(-[0-9]*)?$'")
                      ->latest('id')
                      ->pluck('slug')
                      ->first();

            if ($latestSlug)
            {
                $pieces = explode('-', $latestSlug);

                $number = intval(end($pieces));

                $article['slug'] .= '-' . ($number + 1);
            }
        });
    }
}
