<?php
namespace App\Helpers;

class QueryHelper {

    /**
     * @param $number
     * @return string
     */
    public function parseNumber($number)
    {
        if ($this->isWholeNumber($number))
        {
            return (int) $number;
        }

        return (float) number_format((float) $number, 2, '.', '');
    }

    /**
     * @param $numerator
     * @param $denominator
     * @return null|string
     */
    public function percentage($numerator, $denominator)
    {
        if (!$numerator || !$denominator)
        {
            return null;
        }

        $percentage = ($numerator / $denominator) * 100;

        return $this->parseNumber($percentage);
    }

    /**
     * @param $clicks
     * @param $impressions
     * @return int|null|string
     */
    public function ctr($clicks, $impressions)
    {
        $result = $this->percentage($clicks, $impressions);

        return (is_null($result) ? - 10 : $result);
    }

    /**
     * @param $spending
     * @param $revenue
     * @return int|null|string
     */
    public function acos($spending, $revenue)
    {
        if ($revenue == 0)
        {
            return - 1;
        }

        $result = $this->percentage($spending, $revenue);

        return (is_null($result) ? - 10 : $result);

    }

    /**
     * @param $result
     * @return bool
     */
    private function isWholeNumber($result)
    {
        return is_numeric($result) && strpos($result, '.') === false;
    }

}