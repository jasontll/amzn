<?php
namespace app\Helpers;

use Request;


class LinkHelper {

    /**  Return active state to css class if current page
     *
     * @param array  $routes
     * @param string $active
     * @return null|string
     */
    public static function set_active(Array $routes, $active = 'active')
    {
        //return (Request::route()->getName() == $route ? $active : '');
        foreach ($routes as $route)
        {
            if (Request::route()->getPrefix() == $route)
            {
                return $active;
            }
        }

        return null;
    }

}

