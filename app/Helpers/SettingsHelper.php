<?php

/**
 * How to use:
 * Retrieve:
 * userSettings() - Return all settings
 * userSettings()->all() - Return all settings
 * userSettings($key) - Return setting associated with the key
 * userSettings()->get() - Return all settings
 * userSettings()->get($key) - Return setting associated with the key
 * userSettings()->key - Return setting associated with the key
 *
 * Set:
 * userSettings()->set($key, $value) - Add new attributes to the setting
 *
 * @param null $key
 * @return \App\UserSettings|mixed
 */
function userSettings($key = null)
{
    $settings = app('App\UserSettings');

    return $key ? $settings->get($key) : $settings;
}