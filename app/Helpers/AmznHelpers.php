<?php
use \Illuminate\Http\Request;

if (! function_exists('css_active')) {
    function css_active(Array $routes, $active = 'active')
    {
        foreach ($routes as $route)
        {
            if (app(Request::class)->route()->getPrefix() == $route)
            {
                return $active;
            }
        }
        return null;
    }
}