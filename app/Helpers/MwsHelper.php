<?php
namespace App\Helpers;

use Carbon\Carbon;
use XmlParser;

class MwsHelper {


    /**
     * Get UTC date string for 1 day before or specified date
     *
     * @param      $when
     * @param bool $isBegin
     * @return string
     */
    public function utcDateString($when, $isBegin = true)
    {
        switch ($when)
        {
            case 'yesterday':
                return Carbon::yesterday('America/Los_Angeles')->addMinutes(2)->toIso8601String();
                break;
            case 'endOfYesterday':
                return Carbon::today('America/Los_Angeles')->addMinutes(1)->toIso8601String();
                break;
            case 'now':
                return Carbon::now('America/Los_Angeles')->addMinutes(1)->toIso8601String();
                break;
            case 'today':
                return Carbon::today('America/Los_Angeles')->addMinutes(2)->toIso8601String();
                break;
            default:
                if ($isBegin)
                {
                    return Carbon::parse($when, 'America/Los_Angeles')->addMinutes(2)->toIso8601String();
                }

                return Carbon::parse($when, 'America/Los_Angeles')->addDay(1)->addMinutes(2)->toIso8601String();
        }
    }

    /**
     * Converting XML to array
     *
     * @param $xml
     * @return array
     */
    public function xmlToArray($xml)
    {
        $xml  = XmlParser::extract($xml);
        $xml  = $xml->getContent();
        $json = json_encode($xml);

        return json_decode($json, true)['Message'];
    }

    /**
     * Converting tab-delimited flat file to array
     *
     * @param $flatFile
     * @return array
     */
    public function flatFileToArray($flatFile)
    {
        $flatFile = str_replace("\r", "", $flatFile);
        $rows     = explode("\n", $flatFile);
        $rows     = array_filter($rows);
        $columns  = [];
        $array    = [];

        foreach ($rows as $i => $row)
        {
            $row = explode("\t", $row);
            if ($i == 0)
            {
                $columns = $row;
                foreach ($columns as $ii => $value)
                {
                    $columns[$ii] = str_replace([" ", ".", "-"], "_", $value);
                    $columns[$ii] = studly_case($columns[$ii]);
                }
                continue;
            }
            $_row = [];
            array_walk($row, function ($value, $index) use (&$_row, $columns)
            {
                if ($index < count($columns))
                {
                    $_row[$columns[$index]] = $value;
                }
            }
            );

            array_push($array, $_row);
        }

        return $array;
    }

    /**
     * (Debug only!)
     * Converting tab-delimited flat file to array
     *
     * @param $flatFile
     * @return array
     */
    public function flatFileToArray2($flatFile)
    {
        $flatFile = str_replace("\r", "", $flatFile);
        $rows     = explode("\n", $flatFile);
        $rows     = array_filter($rows);
        $columns  = [];
        $array    = [];

        foreach ($rows as $idx => $row)
        {
            $rows[$idx] = trim($row);
        }

        foreach ($rows as $i => $row)
        {
            $row = explode("\t", $row);
            if ($i == 0)
            {
                $columns = $row;
                foreach ($columns as $ii => $value)
                {
                    $columns[$ii] = str_replace([" ", ".", "-"], "_", $value);
                    $columns[$ii] = studly_case($columns[$ii]);
                }
                continue;
            }


            $_row = [];
            array_walk($row, function ($value, $index) use (&$_row, $columns)
            {
                $_row[$columns[$index]] = $value;
            }
            );
            array_push($array, $_row);
        }

        return $array;
    }

    /**
     * Adjusting time between timezones
     *
     * @param $timestamp
     * @param $fromTz
     * @param $toTz
     * @return static
     */
    public function adjustTime($timestamp, $fromTz, $toTz)
    {
        $date = Carbon::createFromFormat('Y-m-d\TH:i:sO', $timestamp, $fromTz)->setTimezone($toTz);

        return $date;
    }
}