<?php
namespace App;

use Exception;

class UserSettings {

    protected $user;
    protected $settings = [];

    /**
     * Settings constructor.
     *
     * @param array $settings
     * @param User  $user
     */
    public function __construct(array $settings, User $user)
    {
        $this->settings = $settings;
        $this->user     = $user;
    }

    /**
     * UserSettings $settings->get()
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return array_get($this->settings, $key);
    }

    public function set($key, $value)
    {
        $this->settings[$key] = $value;
        $this->persists();
    }

    public function all()
    {
        return $this->settings;
    }

    public function __get($key)
    {
        if ($this->has($key))
        {
            return $this->get($key);
        }
        throw new Exception("The {$key} setting does not exist.");
    }

    public function merge(array $attributes)
    {
        $this->settings = array_merge(
            $this->settings,
            array_only($attributes, array_keys($this->settings))
        );

        return $this->persists();
    }

    protected function persists()
    {
        return $this->user->update(['settings' => $this->settings]);
    }

    public function has($key)
    {
        return array_key_exists($key, $this->settings);
    }
}