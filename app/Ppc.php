<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ppc extends Model {

    protected $fillable = [
        'StartDate',
        'EndDate',
        'MerchantName',
        'SKU',
        'Clicks',
        'Impressions',
        'CTR',
        'Currency',
        'TotalSpend',
        'AvgCPC'
    ];

    protected $dates = [
        'StartDate',
        'EndDate'
    ];
}
