<?php

namespace App\Console\Commands;

use App\Mws\Job;
use Illuminate\Console\Command;

class Update extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AMZN:update {option=default : what to update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update local database with current data from Amazon Seller Central';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $job;

    public function __construct(Job $job)
    {
        parent::__construct();
        $this->job = $job;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $whatToUpdate = $this->argument('option');

        switch ($whatToUpdate)
        {
            case 'orders':
                $this->job->updateOrderStatus();
                break;
            case 'report-availability':
                $this->job->updateReportAvailability();
                break;
            case 'clean':
                $this->job->cleanUpdateQueue();
                $this->info(PHP_EOL . 'Report queue cleaned.' . PHP_EOL);
                break;
            case 'orders-current':
                $this->job->importAndUpdateOrders();
                break;
            default:
                $this->error('The update you are trying to do does not exits!');
                $this->info(PHP_EOL . 'Try options: orders | report-availability | clean' . PHP_EOL);
        }

    }
}
