<?php

namespace App\Console\Commands;

use App\Mws\ProductManager;
use Illuminate\Console\Command;

class Products extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AMZN:products {action}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Managing products from Amazon Seller Central';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $productsManager;

    public function __construct(ProductManager $productManager)
    {
        $this->productsManager = $productManager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');
        switch ($action)
        {
            case 'import':
                $this->productsManager->importLatest();
                break;
            default:
                $this->info(PHP_EOL . 'Available actions: import' . PHP_EOL);
        }
        $this->info('Success:' . $action . ' completed.' . PHP_EOL);
    }
}
