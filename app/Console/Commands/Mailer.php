<?php

namespace App\Console\Commands;

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\InvitationsController;
use Illuminate\Console\Command;

class Mailer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AMZN:mailer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(InvitationsController $invitationsController)
    {
        $invitationsController->sendInvitation('jasonltl@icloud.com', true);
    }
}
