<?php

namespace App\Console\Commands;

use App\Mws\Job;
use Illuminate\Console\Command;

class RequestReport extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AMZN:request-report {type : Report type to request}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Request report on Amazon Seller Central';


    /**
     * RequestReport constructor.
     *
     * @param Job $job
     */
    protected $job;

    public function __construct(Job $job)
    {
        parent::__construct();

        $this->job = $job;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        switch ($type)
        {
            case 'orders':
                $this->job->requestOrderReport();
                break;
            case 'updates':
                $this->job->requestOrdersUpdateReport();
                break;
            default:
                $this->error('Unknown report type!');
                $this->info(PHP_EOL . 'Available options: orders | updates' . PHP_EOL);
        }

        $this->info(PHP_EOL . 'Success:' . $type . ' report requested.' . PHP_EOL);
    }
}
