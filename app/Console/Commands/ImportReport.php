<?php

namespace App\Console\Commands;

use App\Mws\Job;
use Illuminate\Console\Command;

class ImportReport extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AMZN:import {type : Type of report} {--id=default : Report Id for single report}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download and import report from Amazon Seller Central';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $job;

    public function __construct(Job $job)
    {
        parent::__construct();
        $this->job = $job;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        switch ($type)
        {
            case 'ppc':
                $this->job->importPPC();
                $this->info('Success:' . $type . ' report imported.' . PHP_EOL);
                break;
            case 'orders':
                $this->job->importOrders();
                $this->info('Success:' . $type . ' report imported.' . PHP_EOL);
                break;
            case 'single-order':
                $reportId = $this->option('id');
                if ($reportId != 'default')
                {
                    $this->job->importOrdersSingle($reportId);
                    $this->info(PHP_EOL . 'Report: ' . $reportId . ' imported.');
                    break;
                }
                $this->error('Please provide report Id!');
                break;
            case 'single-ppc':
                $reportId = $this->option('id');
                if ($reportId != 'default')
                {
                    $this->job->importPPCSingle($reportId);
                    $this->info(PHP_EOL . 'Report: ' . $reportId . ' imported.' . PHP_EOL);
                    break;
                }
                $this->error('Please provide report Id!');
                break;
            default:
                $this->error('Unknown report type! Use -single if importing single report.');
                $this->info(PHP_EOL . 'Available options: ppc | orders | single-order | single-ppc' . PHP_EOL);
        }


    }
}
