<?php

namespace App\Console;

use App\Console\Commands\ScheduleList;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\ImportReport::class,
        Commands\RequestReport::class,
        Commands\Update::class,
        Commands\Mailer::class,
        Commands\Products::class,
        ScheduleList::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {

        //        $schedule->command('AMZN:request-report orders')->dailyAt('7:00')->timezone('PST');
        //        $schedule->command('AMZN:import orders')->dailyAt('7:07')->timezone('PST');

        $schedule->command('AMZN:import ppc')->dailyAt('7:00')->timezone('America/Los_Angeles');

        /** Cleaning old queue */
        $schedule->command('AMZN:update clean')->mondays()->timezone('America/Los_Angeles');

        $schedule->command('AMZN:update report-availability')->everyMinute()->after(function ()
        {
            Artisan::call('AMZN:import', [
                'type' => 'orders'
            ]);
            Artisan::call('AMZN:update', [
                'option' => 'orders'
            ]);
        });

        $schedule->command('AMZN:update orders-current')->everyTenMinutes();

    }
}
