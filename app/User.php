<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property integer        $id
 * @property string         $username
 * @property string         $email
 * @property string         $password
 * @property string         $remember_token
 * @property string         $settings
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class User extends Authenticatable {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'api_token', 'invitation_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token', 'invitation_token'
    ];

    protected $casts = [
        'settings' => 'json'
    ];

    protected $owners = [
        'jasonltl@icloud.com',
        'andy_liu32@hotmail.com'
    ];


    /**
     * A user can has many article
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }
    
    /**
     * A user has a role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    /**
     * Check user is admin
     *
     * @return bool
     */
    public function isAdmin($email)
    {
        $owners = collect($this->owners);

        return $owners->contains($email);

    }

    /**
     * Get user settings
     *
     * @return UserSettings
     */
    public function settings()
    {
        return new UserSettings($this->settings, $this);
    }

    /**
     * Pending invitations
     *
     * @param $query
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->whereNotNull('invitation_token')
                     ->whereNull('password');
    }

}
