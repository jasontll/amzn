<?php
namespace App\Transformers;

use App\Helpers\QueryHelper;
use App\Order;

class StatsTransformer extends Transformer {

    protected $helper;

    /**
     * StatsTransformer constructor.
     *
     * @param $helper
     */
    public function __construct(QueryHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param Order $orders
     * @return array
     */
    public function transform($salesData)
    {
        return [
            'amount_ytd'     => $salesData['revenue_ytd'],
            'quantity_ytd'   => $salesData['quantity_ytd'],
            'amount_mtd'     => $salesData['revenue_mtd'],
            'quantity_mtd'   => $salesData['quantity_mtd'],
            'amount_wtd'     => $salesData['revenue_wtd'],
            'quantity_wtd'   => $salesData['quantity_wtd'],
            'amount_today'   => $salesData['revenue_today'],
            'quantity_today' => $salesData['quantity_today']
        ];
    }
}
