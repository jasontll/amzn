<?php
namespace App\Transformers;

class ProductTransformer extends Transformer{
    public function transform($product)
    {
        return [
            'product_type' => $product['product_type'],
            'product_title' => $product['product_title'],
            'sku'   => $product['sku'],
            'asin'  => $product['asin']
        ];
    }
}