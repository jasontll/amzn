<?php
namespace App\Transformers;

use App\Helpers\QueryHelper;
use Carbon\Carbon;

class LastWeekPerformanceTransformer extends Transformer {

    protected $helper;

    /**
     * LastWeekPerformanceTransformer constructor.
     */
    public function __construct(QueryHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param $item
     * @return mixed
     */
    public function transform($item)
    {
        return
            [
                'lastWeek_date'              => $item['date'],
                'lastWeek_day'               => Carbon::parse($item['date'])->format('l'),
                'lastWeek_product_type'      => $item['product_type'],
                'lastWeek_total_clicks'      => $this->helper->parseNumber($item['clicks']),
                'lastWeek_total_revenue'     => $this->helper->parseNumber($item['revenue']),
                'lastWeek_total_spend'       => $this->helper->parseNumber($item['spend']),
                'lastWeek_total_quantity'    => $this->helper->parseNumber($item['quantity']),
                'lastWeek_total_impressions' => $this->helper->parseNumber($item['total_impressions']),
                'lastWeek_CTR'               => $this->helper->ctr($item['clicks'], $item['total_impressions']),
                'lastWeek_ACOS'              => $this->helper->acos($item['spend'], $item['revenue'])
            ];
    }
}
