<?php
namespace App\Transformers;

class UserTransformer extends Transformer {

    public function transform($user)
    {
        return [
            'id'             => $user['id'],
            'name'           => sprintf('%s %s', $user['first_name'], $user['last_name']),
            'email'          => $user['email'],
            'is_pending'     => $this->isPending($user['invitation_token']),
            'invitation_url' => $this->invitationUrl($user['invitation_token']),
            'url'            => action('SettingsController@userAccount', ['id' => $user['id']])
        ];
    }

    private function invitationUrl($token)
    {
        if (!$token)
        {
            return null;
        }

        return sprintf('%s?token=%s', url('/register'), $token);
    }

    private function isPending($token)
    {
        if (!$token)
        {
            return false;
        }

        return true;
    }
}