<?php
namespace App\Transformers;

use App\Helpers\QueryHelper;
use Carbon\Carbon;

class SalesPerformanceTransformer extends Transformer {

    protected $helper;

    /**
     * AdsPerformanceTransformer constructor.
     *
     * @param $helper
     */
    public function __construct(QueryHelper $helper)
    {
        $this->helper = $helper;
    }

    public function transform($item)
    {
        return
            [
                'date'              => $item['date'],
                'day'               => Carbon::parse($item['date'])->format('l'),
                'product_type'      => $item['product_type'],
                'total_clicks'      => $this->helper->parseNumber($item['clicks']),
                'total_revenue'     => $this->helper->parseNumber($item['revenue']),
                'total_spend'       => $this->helper->parseNumber($item['spend']),
                'total_quantity'    => $this->helper->parseNumber($item['quantity']),
                'total_impressions' => $this->helper->parseNumber($item['total_impressions']),
                'CTR'               => $this->helper->ctr($item['clicks'], $item['total_impressions']),
                'ACOS'              => $this->helper->acos($item['spend'], $item['revenue'])
            ];
    }
}