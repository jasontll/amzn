<?php
namespace App\Transformers;

abstract class Transformer {

    /**
     * @param array $items
     * @param null  $keyModifier
     * @return array
     */

    public function transformCollections(array $items, $keyModifier = null)
    {
        if ($keyModifier)
        {
            $items = $this->injectKeyModifier($items, $keyModifier);
        }

        return array_map([$this, 'transform'], $items);
    }

    /**
     * @param $item
     * @return mixed
     */
    public abstract function transform($item);

    /**
     * @param      $key
     * @param null $modifier
     * @return string
     */
    public function modifyKey($key, $modifier = null)
    {
        if ($modifier)
        {
            if ($modifier['mode'] == 'prefix')
            {
                return sprintf('%s%s', $modifier['key'], $key);
            }

            if ($modifier['mode'] == 'suffix')
            {
                return sprintf('%s%s', $key, $modifier['key']);
            }
        }

        return $key;
    }
    
    /**
     * @param array $items
     * @return array|\Illuminate\Support\Collection
     */
    private function injectKeyModifier(array $items, $keyModifier)
    {
        foreach ($items as $item){
            $item['keyModifier'] = $keyModifier;
        }
        return $items;
    }

}