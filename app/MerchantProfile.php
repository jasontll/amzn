<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantProfile extends Model {

    protected $fillable = [
        'seller_id',
        'mws_auth_token',
        'aws_access_key_id',
        'mws_secret_key',
        'settings'
    ];

    protected $casts = [
        'settings' => 'json'
    ];

    /**
     *  One to many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
