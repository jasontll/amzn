<?php

namespace App\Providers;

use App\Tag;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeSingleArticleSidebar();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Compose single article sidebar
     */
    private function composeSingleArticleSidebar()
    {
        View()->composer('articles._single-sidebar', 'App\Http\Composers\SingleArticleSidebarComposer');
    }
}
