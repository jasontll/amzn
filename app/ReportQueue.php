<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportQueue extends Model
{
    protected $fillable = [
        'ReportRequestId',
        'ReportId',
        'ReportType',
        'Status'
    ];
}
