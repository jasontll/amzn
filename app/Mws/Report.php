<?php
namespace App\Mws;

use App\Helpers\MwsHelper;
use App\ReportQueue;
use Peron\AmazonMws\AmazonReport;
use Peron\AmazonMws\AmazonReportAcknowledger;
use Peron\AmazonMws\AmazonReportList;
use Peron\AmazonMws\AmazonReportRequest;
use Peron\AmazonMws\AmazonReportRequestList;

class Report {

    protected $amzReportRequest;
    protected $amzReportList;
    protected $amzReport;
    protected $amzReportAcknowledger;
    protected $amzReportRequestList;
    protected $helper;

    public function __construct(MwsHelper $helper)
    {
        $this->amzReportList         = new AmazonReportList('GoFurball');
        $this->amzReportRequest      = new AmazonReportRequest('GoFurball');
        $this->amzReport             = new AmazonReport('GoFurball');
        $this->amzReportAcknowledger = new AmazonReportAcknowledger('GoFurball');
        $this->amzReportRequestList  = new AmazonReportRequestList('GoFurball');
        $this->helper                = $helper;
    }

    /**
     * Request report from Amazon Seller Central and queue it up
     *
     * @param        $reportType
     * @param string $begin - date yyyy-mm-dd
     * @param string $end   - date yyyy-mm-dd
     */
    public function requestReport($reportType, $begin = 'yesterday', $end = 'endOfYesterday')
    {
        $this->amzReportRequest->setReportType($reportType);
        $this->amzReportRequest->setTimeLimits($this->helper->utcDateString($begin), $this->helper->utcDateString($end));
        $this->amzReportRequest->requestReport();
        $reportRequestId = $this->amzReportRequest->getReportRequestId();

        ReportQueue::firstOrCreate(
            [
                'ReportRequestId' => $reportRequestId,
                'ReportType'      => $reportType
            ]
        );

    }

    /**
     * Acknowledge the report after it's been imported
     *
     * @param      $reportIdList
     * @param bool $acknowledged
     * @return mixed
     */
    public function acknowledge($reportIdList, $acknowledged = true)
    {
        $this->amzReportAcknowledger->setReportIds($reportIdList);
        $this->amzReportAcknowledger->setAcknowledgedFilter($acknowledged);
        $this->amzReportAcknowledger->acknowledgeReports();
        $acknowledgedReportList = $this->amzReportAcknowledger->getList();

        return collect($acknowledgedReportList);
    }

    /**
     * Get report from Amazon Seller Central
     *
     * @param $reportID
     * @return mixed
     */
    public function getReport($reportID)
    {
        $this->amzReport->setReportId($reportID);
        $flatFile = $this->amzReport->fetchReport();
        $report   = $this->helper->flatFileToArray($flatFile);

        return collect($report);
    }

    /**
     * Check pending report and update availability in report queue.
     */
    public function updateReportAvailability()
    {
        $reportRequestIdList = ReportQueue::where('Status', 'Pending')->pluck('ReportRequestId')->toArray();

        if (sizeof($reportRequestIdList) > 0)
        {
            $this->amzReportRequestList->setRequestIds($reportRequestIdList);
            $this->amzReportRequestList->fetchRequestList();
            $reportList = $this->amzReportRequestList->getList();
            $reportList = collect($reportList);

            $reportList->each(function ($report, $key)
            {
                if ($report['ReportProcessingStatus'] == '_DONE_')
                {
                    ReportQueue::where('Status', 'Pending')
                               ->where('ReportRequestId', $report['ReportRequestId'])
                               ->update(['ReportId' => $report['GeneratedReportId'], 'Status' => 'Ready']);
                }
            }
            );
        }
    }

    /**
     * Get most recent report Id for report type
     *
     * @param  $reportType
     * @return mixed
     */
    public function latestReportId($reportType)
    {
        $list     = $this->reportList($reportType, false);
        $reportId = $list->first()['ReportId'];

        if (!$reportId)
        {
            return false;
        }

        return $reportId;
    }

    /**
     * Get the report list
     *
     * @param null $reportTypes
     * @param bool $useToken
     * @param bool $acknowledged
     * @return mixed
     */
    public function reportList($reportTypes = null, $useToken = false, $acknowledged = false)
    {
        //_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_
        //_GET_PADS_PRODUCT_PERFORMANCE_OVER_TIME_DAILY_DATA_TSV_
        //_GET_MERCHANT_LISTINGS_DATA_
        //_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_
        //_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_

        if ($reportTypes != null)
        {
            $this->amzReportList->setReportTypes($reportTypes);
        }
        if ($useToken)
        {
            $this->amzReportList->setUseToken();
        }

        $this->amzReportList->setAcknowledgedFilter($acknowledged);

        $this->amzReportList->fetchReportList();
        $reportList = $this->amzReportList->getList();

        return collect($reportList);
    }
}