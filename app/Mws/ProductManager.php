<?php
namespace App\Mws;

use App\Helpers\MwsHelper;
use App\Product;
use Peron\AmazonMws\AmazonInventoryList;

class ProductManager {

    protected $amzInventory;
    protected $report;
    protected $helper;

    public function __construct(MwsHelper $helper, Report $report)
    {
        $this->amzInventory = new AmazonInventoryList('GoFurball');
        $this->report       = $report;
        $this->helper       = $helper;
    }

    public function allProducts()
    {
        $reportId = $this->report->latestReportId('_GET_MERCHANT_LISTINGS_DATA_');
        $products = $this->report->getReport($reportId);

        return $products;
    }

    public function importLatest()
    {
        $products = $this->allProducts();
        $products->each(function ($product, $key)
        {
            Product::createIfFirst([
                'sku'  => $product['SellerSku'],
                'asin' => $product['Asin1']
            ], [
                'product_title' => $product['ItemName'],
                'sku'      => $product['SellerSku'],
                'asin'     => $product['Asin1']
            ]);
        });

        return $products;
    }
}