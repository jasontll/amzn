<?php
namespace App\Mws;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Peron\AmazonMws\AmazonOrderList;
use Peron\AmazonMws\AmazonOrder;
use Peron\AmazonMws\AmazonFulfillmentOrder;
use Peron\AmazonMws\AmazonOrderItemList;
use Peron\AmazonMws\AmazonFeedResult;

class Order {

    public function __construct()
    {

    }

    private function listOrders()
    {
        $amz   = new AmazonOrderList('GoFurball');
        $after = Carbon::today('PST')->toIso8601String();
        $amz->setLimits('Created', $after);
        $amz->setOrderStatusFilter(['Shipped']);
        $amz->setUseToken();
        $amz->fetchOrders();
        //$amz = collect($amz->getList());
        //$amz->getList();
        return $amz;

    }

    private function OrderDetails()
    {
        $orders = $this->listOrders();

        $orderDetails = new Collection();

        foreach ($orders as $order)
        {
            $orderDetail = $order->getData();
            $orderDetails->push($orderDetail);
        }

        return $orderDetails;
    }

    public function getOrder()
    {
        $amz = new AmazonOrderList('GoFurball', '105-1848158-9099400');

        return $amz;
    }

    public function show()
    {
        $orders = $this->listOrders();

        return $orders;
    }
}