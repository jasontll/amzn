<?php
namespace App\Mws;

use App\Helpers\MwsHelper;
use App\Order;
use App\Ppc;
use App\ReportQueue;
use Carbon\Carbon;

class Job {

    protected $report;
    protected $helper;
    protected $query;

    public function __construct(Report $report, MwsHelper $helper)
    {
        $this->report = $report;
        $this->helper = $helper;
    }

    /**
     * Schedule report for orders and update
     */
    public function importAndUpdateOrders()
    {
        $this->report->requestReport('_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_', 'today', 'now');
        sleep(300);
        $this->report->requestReport('_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_', 'today', 'now');
    }

    /**
     * Update orders data
     */
    public function updateOrderStatus()
    {
        $reportId = $this->getQueuedReport('_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_');

        if ($reportId)
        {
            $updatedOrders = $this->report->getReport($reportId);

            $updatedOrders->each(function ($order, $key)
            {

                $lastUpdateDate = $this->helper->adjustTime($order['LastUpdatedDate'], 'UTC', 'America/Los_Angeles');

                Order::where('AmazonOrderId', $order['AmazonOrderId'])
                     ->where('ASIN', $order['Asin'])
                     ->where('LastUpdatedDate', '<', $lastUpdateDate)
                     ->update([
                             'OrderStatus'           => $order['OrderStatus'],
                             'FulfillmentChannel'    => $order['FulfillmentChannel'],
                             'LastUpdatedDate'       => $lastUpdateDate,
                             'SalesChannel'          => $order['SalesChannel'],
                             'ShipServiceLevel'      => $order['ShipServiceLevel'],
                             'ShipCity'              => $order['ShipCity'],
                             'ShipState'             => $order['ShipState'],
                             'ShipPostalCode'        => $order['ShipPostalCode'],
                             'ShipCountry'           => $order['ShipCountry'],
                             'Quantity'              => $order['Quantity'],
                             'Currency'              => $order['Currency'],
                             'ItemPrice'             => $order['ItemPrice'],
                             'ItemTax'               => $order['ItemTax'],
                             'ShippingPrice'         => $order['ShippingPrice'],
                             'ShippingTax'           => $order['ShippingTax'],
                             'GiftWrapPrice'         => $order['GiftWrapPrice'],
                             'GiftWrapTax'           => $order['GiftWrapTax'],
                             'ItemPromotionDiscount' => $order['ItemPromotionDiscount'],
                             'ShipPromotionDiscount' => $order['ShipPromotionDiscount'],
                             'PromotionIds'          => $order['PromotionIds']
                         ]
                     );
            }
            );

            $this->report->acknowledge($reportId);

            $this->updateQueueStatus($reportId);
        }
    }

    /**
     * Get report that are ready for process
     *
     * @param $reportType
     * @return bool
     */
    private function getQueuedReport($reportType)
    {
        $readyReport = ReportQueue::where('Status', 'Ready')
                                  ->where('ReportType', $reportType)
                                  ->first();
        if (!$readyReport)
        {
            return false;
        }

        return $readyReport->ReportId;
    }

    /**
     * Update report queue status
     *
     * @param        $reportId
     * @param string $status
     */
    public function updateQueueStatus($reportId, $status = 'Complete')
    {
        ReportQueue::where('ReportId', $reportId)
                   ->update([
                           'Status' => $status
                       ]
                   );
    }

    /**
     *  Insert orders into database
     */
    public function importOrders()
    {
        $reportId = $this->getQueuedReport('_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_');

        if ($reportId)
        {
            $this->processOrder($reportId);

            $this->report->acknowledge($reportId);
            $this->updateQueueStatus($reportId);
        }

    }

    /**
     * @param $reportId
     */
    public function processOrder($reportId)
    {
        $orders = $this->report->getReport($reportId);

        $orders->each(function ($order, $key)
        {
            Order::createIfFirst([
                'AmazonOrderId' => $order['AmazonOrderId'],
                'PurchaseDate'  => $this->helper->adjustTime($order['PurchaseDate'], 'UTC', 'America/Los_Angeles'),
                'SKU'           => $order['Sku']
            ], [
                'AmazonOrderId'         => $order['AmazonOrderId'],
                'PurchaseDate'          => $this->helper->adjustTime($order['PurchaseDate'], 'UTC', 'America/Los_Angeles'),
                'LastUpdatedDate'       => $this->helper->adjustTime($order['LastUpdatedDate'], 'UTC', 'America/Los_Angeles'),
                'OrderStatus'           => $order['OrderStatus'],
                'SKU'                   => $order['Sku'],
                'ASIN'                  => $order['Asin'],
                'FulfillmentChannel'    => $order['FulfillmentChannel'],
                'SalesChannel'          => $order['SalesChannel'],
                'ShipServiceLevel'      => $order['ShipServiceLevel'],
                'ShipCity'              => $order['ShipCity'],
                'ShipState'             => $order['ShipState'],
                'ShipPostalCode'        => $order['ShipPostalCode'],
                'ShipCountry'           => $order['ShipCountry'],
                'Quantity'              => $order['Quantity'],
                'Currency'              => $order['Currency'],
                'ItemPrice'             => $order['ItemPrice'],
                'ItemTax'               => $order['ItemTax'],
                'ShippingPrice'         => $order['ShippingPrice'],
                'ShippingTax'           => $order['ShippingTax'],
                'GiftWrapPrice'         => $order['GiftWrapPrice'],
                'GiftWrapTax'           => $order['GiftWrapTax'],
                'ItemPromotionDiscount' => $order['ItemPromotionDiscount'],
                'ShipPromotionDiscount' => $order['ShipPromotionDiscount'],
                'PromotionIds'          => $order['PromotionIds']
            ]);
        }
        );
    }

    /**
     * Insert PPC into database
     */
    public function importPPC()
    {
        $reportId = $this->report->latestReportId('_GET_PADS_PRODUCT_PERFORMANCE_OVER_TIME_DAILY_DATA_TSV_');

        if ($reportId)
        {
            $ppcs = $this->report->getReport($reportId);

            $ppcs->each(function ($ppc, $key)
            {
                Ppc::firstOrCreate([
                        'StartDate'    => Carbon::parse($ppc['StartDate']),
                        'EndDate'      => Carbon::parse($ppc['EndDate']),
                        'MerchantName' => $ppc['MerchantName'],
                        'SKU'          => $ppc['SKU'],
                        'Clicks'       => $ppc['Clicks'],
                        'Impressions'  => $ppc['Impressions'],
                        'CTR'          => $ppc['CTR'],
                        'Currency'     => $ppc['Currency'],
                        'TotalSpend'   => $ppc['TotalSpend'],
                        'AvgCPC'       => $ppc['AvgCPC']
                    ]
                );
            }
            );
            $this->report->acknowledge($reportId);
        }

    }

    /**
     * Insert single PPC report into database
     *
     * @param $reportId
     */
    public function importPPCSingle($reportId)
    {
        $ppcs = $this->report->getReport($reportId);

        $ppcs->each(function ($ppc, $key)
        {
            Ppc::firstOrCreate([
                    'StartDate'    => Carbon::parse($ppc['StartDate']),
                    'EndDate'      => Carbon::parse($ppc['EndDate']),
                    'MerchantName' => $ppc['MerchantName'],
                    'SKU'          => $ppc['SKU'],
                    'Clicks'       => $ppc['Clicks'],
                    'Impressions'  => $ppc['Impressions'],
                    'CTR'          => $ppc['CTR'],
                    'Currency'     => $ppc['Currency'],
                    'TotalSpend'   => $ppc['TotalSpend'],
                    'AvgCPC'       => $ppc['AvgCPC']
                ]
            );
        }
        );
        $this->report->acknowledge($reportId);

    }

    /**
     * Insert single Orders report into database
     *
     * @param $reportId
     */
    public function importOrdersSingle($reportId)
    {
        $this->processOrder($reportId);
        $this->report->acknowledge($reportId);
        $this->updateQueueStatus($reportId);
    }

    /**
     * Request Orders report from MWS
     */
    public function requestOrderReport()
    {
        //        '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_'
        //        '_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_'
        $this->report->requestReport('_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_');
    }

    /**
     * Request Orders Update report from MWS
     */
    public function requestOrdersUpdateReport()
    {
        //_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_
        $this->report->requestReport('_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_', 'today', 'now');
    }

    /**
     *  Check report availability for queueing
     */
    public function updateReportAvailability()
    {
        $this->report->updateReportAvailability();
    }

    /**
     *  Cleaning queue that's 2 weeks and older
     */
    public function cleanUpdateQueue()
    {
        ReportQueue::where(['Status' => 'Complete'])
                   ->where('created_at', '<=', Carbon::today())
                   ->delete();
    }
}