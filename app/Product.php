<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $fillable = [
        'product_title',
        'sku',
        'asin',
        'category'
    ];

    public static function createIfFirst(array $attributes, array $values)
    {
        if (is_null($instance = Product::where($attributes)->first()))
        {
            $instance = Product::create($values);
        }

        return $instance;
    }
}
