<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Response;
use Symfony\Component\HttpFoundation\Response as IlluminateResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        ApiException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function report(Exception $e)
    {
        if ($e instanceof ModelNotFoundException)
        {
            return Response::json([
                ['message' => 'Resource not found.']
            ], 404);
        }

        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($request->wantsJson())
        {
            return $this->handle($request, $e);
        }

        return parent::render($request, $e);

    }

    /**
     * Convert the Exception into a JSON HTTP Response
     *
     * @param           $request
     * @param Exception $e
     * @return JsonResponse
     */
    private function handle($request, Exception $e)
    {

        if ($e instanceOf ApiException)
        {
            $data   = $e->toArray();
            $status = $e->getStatus();
        }

        if ($e instanceOf NotFoundHttpException)
        {
            $data = array_merge([
                'id'        => 'not_found',
                'status'    => IlluminateResponse::HTTP_NOT_FOUND,
                'exception' => class_basename($e)
            ], config('errors.not_found'));

            $status = IlluminateResponse::HTTP_NOT_FOUND;
        }

        if ($e instanceOf MethodNotAllowedHttpException)
        {
            $data = array_merge([
                'id'        => 'method_not_allowed',
                'status'    => IlluminateResponse::HTTP_METHOD_NOT_ALLOWED,
                'exception' => class_basename($e)
            ], config('errors.method_not_allowed'));

            $status = IlluminateResponse::HTTP_METHOD_NOT_ALLOWED;
        }

        if ($e instanceof TokenMismatchException)
        {
            $data = array_merge([
                'id'        => 'precondition_failed',
                'status'    => IlluminateResponse::HTTP_PRECONDITION_FAILED,
                'exception' => class_basename($e)
            ], config('errors.precondition_failed'));

            $status = IlluminateResponse::HTTP_PRECONDITION_FAILED;
        }

        if ($e instanceof ValidationException)
        {
            $data = [
                'id'        => 'form_validation_error',
                'status'    => IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY,
                'message'   => $e->getMessage(),
                'detail'    => json_decode($e->getResponse()->getContent()),
                'exception' => class_basename($e)
            ];

            $status = IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY;
        }

        if ($e instanceof HttpResponseException)
        {
            $data   = [
                'id'        => 'http_response_error',
                'status'    => $e->getResponse()->getStatusCode(),
                'message'   => 'Form validation error.',
                'detail'    => json_decode($e->getResponse()->getContent()),
                'exception' => class_basename($e),

            ];
            $status = $e->getResponse()->getStatusCode();
        } else
        {
            $exception = class_basename($e);

            if (config('app.debug'))
            {
                $exception = sprintf('%s: %s', class_basename($e), $e->getMessage());
            }

            $data = array_merge([
                'id'        => 'internal_server_error',
                'status'    => IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR,
                'exception' => $exception
            ], config('errors.internal_server_error'));

            $status = IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        return response()->json($data, $status);
    }
}
