<?php
namespace App\Exceptions;

use Illuminate\Http\Response;

class NotFoundException extends ApiException {

    /**
     * @var string
     */
    protected $status = Response::HTTP_NOT_FOUND;

    /**
     * @return void
     */
    public function __construct()
    {
        $message = $this->build(func_get_args());

        parent::__construct($message);
    }
}