@extends('layouts.app')
@section('content')
    <div id="app">
        <button class="btn btn-default" v-on:click="invitationModal = true">Show modal</button>

        <modal :show.sync="invitationModal">
            <h3 slot="header">Send Invitation</h3>
            <div slot="form">
                @include('settings._invitation-form')
            </div>
        </modal>

    </div>
@endsection
