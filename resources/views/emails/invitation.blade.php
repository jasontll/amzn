@extends('emails.layout')
@section('pre-header')
    Trendelo.us invitation
@endsection

@section('content')
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <!-- COPY -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" style="font-size: 18px; font-family: Lato, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding">You're invited...</td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Lato, Arial, sans-serif; color: #666666;" class="padding">Hi {{ $invitation->first_name }}, you have been invited to use the Trendelous backend system. Please follow the link below to complete your registration.</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <!-- BULLETPROOF BUTTON -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" style="padding-top: 25px;" class="padding">
                            <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                <tr>
                                    <td align="center" style="border-radius: 3px;" bgcolor="#256F9C">
                                        <a href="{{ url('/register')}}?token={{$invitation->invitation_token}}" target="_blank" style="font-size: 16px; font-family: Lato, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; border-radius: 3px; padding: 15px 25px; border: 1px solid #256F9C; display: inline-block;" class="mobile-button">Register Now &rarr;</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection