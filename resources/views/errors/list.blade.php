@if ($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <p><strong>Error!</strong> {{$error}}</p>
        @endforeach
    </div>
@endif
