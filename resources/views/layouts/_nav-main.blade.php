<ul class="nav navbar-nav">
    <li class="{{LinkHelper::set_active(['dashboard'])}}">
        <a href="{{ route('dashboard') }}"><i class="icon-bar-chart"></i> Dashboard</a>
    </li>
    <li>
        <a href="#"><i class="icon-target"></i> Campaign</a>
    </li>
    <li>
        <a href="#"><i class="icon-rocket"></i> Keywords</a>
    </li>
</ul>