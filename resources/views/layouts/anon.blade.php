<!DOCTYPE html>
<html>
    <head>
        <title>Trendelo.us | AMZN</title>
        <link rel="stylesheet" href="/vendor/global/css/components-md.css">
        <link rel="stylesheet" href="/vendor/global/plugins/simple-line-icons/simple-line-icons.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                /*color: #B0BEC5;*/
                display: table;
                font-family: 'Lato';
                background-color: #eef1f5;
            }

            .container {
                display: table-cell;
                vertical-align: middle;
            }

            .title {
                text-align: center;
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="page-container">
                <h1 class="title" style="color: #f01331">Trendelo.us</h1>
                @yield('content')
            </div>
        </div>
    </body>
</html>
