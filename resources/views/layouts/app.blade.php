<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
<![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js">
<![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8"/>
        <title>Trendelous | AMZN</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="Amazon Seller Management Console" name="description"/>
        <meta content="Jason Lee" name="author"/>
        <meta name="_token" content="{{ csrf_token() }}">
        <!-- Styles -->
        {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--}}
        <link rel="stylesheet" href="{{URL::asset('/vendor/global/plugins/pace/themes/pace-theme-flash.css')}}">
        <script src="{{URL::asset('/vendor/global/plugins/pace/pace.min.js')}}"></script>
        <link href="{{ elixir('css/compiled.css') }}" rel="stylesheet">
        @yield('style')

    </head>
    <body id="app" class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-md">
    @include('layouts._header')
    <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('layouts._left-sidebar')
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                {{--<!-- BEGIN CONTENT BODY -->--}}
                {{--<!-- BEGIN PAGE HEAD-->--}}
                {{--<div class="page-head">--}}
                {{--<div class="container">--}}
                {{--<!-- BEGIN PAGE TITLE -->--}}
                {{--<div class="page-title">--}}
                {{--<h1>Blank Page</h1>--}}
                {{--</div>--}}
                {{--<!-- END PAGE TITLE -->--}}
                {{--<!-- BEGIN PAGE TOOLBAR -->--}}
                {{--@include('layouts._page-toolbar')--}}
                {{--<!-- END PAGE TOOLBAR -->--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!-- END PAGE HEAD-->--}}
                {{--<!-- BEGIN PAGE CONTENT BODY -->--}}
                <div class="page-content">
                    {{--<!-- BEGIN PAGE BREADCRUMBS -->--}}
                    {{--@include('layouts._page-breadcrumbs')--}}
                    {{--<!-- END PAGE BREADCRUMBS -->--}}
                    {{--<!-- BEGIN PAGE CONTENT INNER -->--}}
                    <div class="page-content-inner">
                        @yield('content')
                    </div>
                    {{--<!-- END PAGE CONTENT INNER -->--}}
                </div>
                {{--<!-- END PAGE CONTENT BODY -->--}}
                {{--<!-- END CONTENT BODY -->--}}
            </div>
            <!-- END CONTENT -->
            {{--<!-- BEGIN QUICK SIDEBAR -->--}}
            {{--@include('layouts._page-sidebar')--}}
            {{--<!-- END QUICK SIDEBAR -->--}}
        </div>
    {{--<!-- END CONTAINER -->--}}

    {{--<!-- BEGIN FOOTER -->--}}
    @include('layouts._footer')
    {{--<!-- END FOOTER -->--}}

    <!-- JavaScripts --><!--[if lt IE 9]>
        <script src="/vendor/global/plugins/respond.min.js"></script>
        <script src="/vendor/global/plugins/excanvas.min.js"></script><![endif]-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        {{--Place to attach scripts with directive--}}
        <footer></footer>
        {{--Place to attach scripts with directive--}}
        @yield('page_scripts')
        <script src="{{ elixir('js/app.js') }}"></script>
        @yield('footer')
    </body>
</html>
