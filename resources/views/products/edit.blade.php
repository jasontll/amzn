@extends('layouts.app')
@section('page-top')
    <div class="t-breadcrumb"><h4><i class="icon-tag"></i>
            <a href="{{url()->current()}}">Products</a> / {{$product->product_title}}</h4>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-7 col-md-offset-1">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-tag"></i>
                        <span class="caption-subject Capitalize"> {{$product->product_title}}</span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="Fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form id="product-edit" method="POST" action="/api/v1/products/{{$product->asin}}" v-ajax complete="reload">
                        {{method_field('PUT')}}
                        <div class="form-body">
                            <div class="form-group form-md-line-input">
                                <input type="text" class="form-control" id="product_title" name="product_title" value="{{$product->product_title}}" required>
                                <label for="product_title">Title</label>
                            </div>
                            <div class="form-group form-md-line-input">
                                <input type="text" class="form-control" id="internal_title" name="internal_title" value="{{$product->internal_title}}">
                                <label for="internal_title">Internal Title</label>
                                <span class="help-block">For internal use only</span>
                            </div>
                            <div class="form-group form-md-line-input">
                                <textarea class="form-control" id="description" name="description" value="{{$product->description}}" v-medium-edit>{{$product->description}}</textarea>
                                <label for="description">Description</label>
                                <span class="help-block">For internal use only</span>
                            </div>
                            <input type="hidden" name="product_type" value="{{$product->product_type}}">
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="portlet light bg-grey-cararra">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-red-sunglo"></i>
                        <span class="caption-subject"> Stock Control ID</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <h5>Asin: {{$product->asin}}</h5>
                    <h5>SKU/UPC: {{$product->sku}}</h5>
                </div>
            </div>
            <product-type-select></product-type-select>
        </div>
    </div>
@endsection
