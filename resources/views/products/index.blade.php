@extends('layouts.app')

@section('page-top')
    <div class="t-breadcrumb"><h4><i class="icon-tag"></i> <a href="{{url()->current()}}">Products</a></h4></div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <h2>Products</h2>
            <p>You can manage your products here.</p>
        </div>
        <div class="col-md-9">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-tag"></i> <span class="caption-subject bold uppercase"> Products</span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="actions">
                        <form id="product-refresh" action="/api/v1/products/import" v-ajax>
                            {{method_field('GET')}}
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-circle btn-primary">
                                <i class="fa fa-refresh"></i><span class="hidden-xs">Refresh</span>
                            </button>
                        </form>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <products-grid
                            url="/api/v1/products"
                            :columns="[{name: 'product_type', text: 'Type'},{name: 'product_title', text: 'Title'},{name: 'asin', text: 'ASIN'}]">
                    </products-grid>
                </div>
            </div>
        </div>
    </div>
@endsection
