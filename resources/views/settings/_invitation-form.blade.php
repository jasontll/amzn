<form id="invitation" method="POST" action="/api/v1/users" v-ajax complete="Invitation sent!">
    <div class="row modal-body">
        <div class="form-body">
            <div class="col-md-6">
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="first_name" name="first_name" required>
                    <label for="first_name">First Name</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="last_name" name="last_name" required>
                    <label for="last_name">Last Name</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="email" class="form-control" id="email" name="email" required>
                    <label for="email">Email</label> <span class="help-block">Make sure the email is correct</span>
                </div>
            </div>
            {{--{{csrf_field()}}--}}
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Send Invite</button>
        <action-link action="modal">Cancel</action-link>
    </div>
</form>
