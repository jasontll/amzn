<h4 class="form-section">Basic Information</h4>
<form id="user-account" method="POST" action="/api/v1/users/{{$user->id}}" v-ajax>
    {{method_field('PUT')}}
    {{Form::hidden('id', $user->id)}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{$user->first_name}}" required>
                    <label class="sbold" for="first_name">First Name</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{$user->last_name}}" required>
                    <label class="sbold" for="last_name">Last Name</label>
                </div>
            </div>
        </div>
        <div class="row margin-top-20">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" required>
                    <label class="sbold" for="email">Email Address</label>
                    <span class="help-block help-default">This is also your login name.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn btn-primary" type="submit">Update</button>
    </div>
</form>
<hr>
<h4 class="form-section">Password</h4>
<form id="user-password" method="POST" action="/api/v1/users/{{$user->id}}/password" v-ajax>
    {{method_field('PUT')}}
    {{Form::hidden('id', $user->id)}}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-md-line-input">
                <input type="password" class="form-control" id="password" name="password" required minlength="10">
                <label for="password">New Password</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-md-line-input">
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required minlength="10">
                <label for="password_confirmation">Confirm Password</label>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button type="submit" class="btn btn-primary">Update Password</button>
    </div>
</form>