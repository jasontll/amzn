@extends('layouts.app')
@section('page-top')
    <div class="t-breadcrumb"><h4><i class="icon-settings"></i> <a href="{{url()->current()}}">Settings</a> / User Account</h4></div>
@endsection
@section('content')
    <div class="col-md-4">
        <h2>Account Information</h2>
        <p>You can manage your account information here:</p>
        <ul>
            <li>Update Names</li>
            <li>Update Email</li>
            <li>Update Password</li>
        </ul>
    </div>
    <div class="col-md-8">
        <div class="portlet light">
            <div class="portlet-body form">
                @include('settings._user-account-form')
            </div>
        </div>
    </div>
@endsection
