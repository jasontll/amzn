@extends('layouts.app')
@section('page-top')
    <div class="t-breadcrumb"><h4><i class="icon-settings"></i> <a href="{{url()->current()}}">Settings</a></h4></div>
@endsection
@section('content')
    <div class="col-md-4">
        <h2>User Management</h2>
        <p>You can manage your users here.</p>
        <ul>
            <li>Add new members to the team</li>
            <li>Remove user</li>
        </ul>
    </div>
    <div class="col-md-8">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-users"></i> <span class="caption-subject bold uppercase"> Staff Members</span>
                    <span class="caption-helper"></span>
                </div>
                <div class="actions">
                    <button class="btn btn-circle btn-primary" @click="invitationModal = true"><i class="fa fa-plus"></i> Add </a></button>
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <members-grid url="/api/v1/users" :columns="['name','email']"></members-grid>
            </div>
        </div>
    </div>
    <modal :show.sync="invitationModal">
        <h3 slot="header">Send Invitation</h3>
        <div slot="form">
            @include('settings._invitation-form')
        </div>
    </modal>
@endsection