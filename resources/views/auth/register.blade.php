@extends('layouts.anon')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-blue-steel text-uppercase">Complete Registration</div>
                </div>
                <div class="portlet-body form">
                    <p>Hi {{session('user')->first_name}},</p>
                    <p>You have been invited to create an account with Trendelo.us. Please finalize your account details here. <strong>Once completed you will have access to the Trendelo.us business backend.</strong></p>
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['url' => '/register', 'class' => 'form-horizontal margin-top-40 margin-bottom-40']) !!}
                            <div class="form-group form-md-line-input {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                {!! Form::label('first_name', 'First Name', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::text('first_name', session('user')->first_name, ['class'=>'form-control']) !!}
                                    <div class="form-control-focus"></div>
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group form-md-line-input {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                {!! Form::label('last_name', 'Last Name', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::text('last_name', session('user')->last_name, ['class'=>'form-control']) !!}
                                    <div class="form-control-focus"></div>
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                                {!! Form::label('email', 'Email Address', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::email('email', session('user')->email, ['class'=>'form-control']) !!}
                                    <div class="form-control-focus"></div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group form-md-line-input {{ $errors->has('password') ? 'has-error' : '' }}">
                                {!! Form::label('password', 'Password', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                    <div class="form-control-focus"></div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            {!! Form::hidden('invitation_token', session('user')->invitation_token) !!}
                            <div class="form-group form-md-line-input {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                {!! Form::label('password_confirmation', 'Confirm Password', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                    <div class="form-control-focus"></div>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-actions right">
                                <button type="submit" class="btn btn-lg blue"><i class="icon-rocket"></i> SIGN UP
                                </button>
                            </div>
                            {!! Form::close() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
