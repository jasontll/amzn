@extends('layouts.anon')

<!-- Main Content -->
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-blue-steel text-uppercase">Reset Password</div>
                </div>
                <div class="portlet-body form">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        {!! Form::open(['url' => '/password/email', 'class' => 'form-horizontal' ]) !!}
                            <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                                    <div class="form-control-focus"></div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group margin-top-40">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-envelope"></i>Send Password Reset Link
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
