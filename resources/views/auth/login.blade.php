@extends('layouts.anon')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-blue-steel text-uppercase">Sign In</div>
                </div>
                <div class="portlet-body form">
                    {{Form::open(['url' => '/login', 'class' => 'form-horizontal margin-top-40 margin-bottom-40'])}}
                    <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                        {!! Form::label('email', 'Email', ['class'=>'col-md-2 control-label']) !!}
                        <div class="col-md-8">
                            {!! Form::email('email', null, ['class'=>'form-control']) !!}
                            <div class="form-control-focus"></div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-md-line-input {{ $errors->has('password') ? 'has-error' : '' }}">
                        {!! Form::label('password', 'Password', ['class'=>'col-md-2 control-label']) !!}
                        <div class="col-md-8">
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                            <div class="form-control-focus"></div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <div class="col-md-offset-2 col-md-4">
                            <div class="md-checkbox-list">
                                <div class="md-checkbox">
                                    <input name="remember" type="checkbox" id="remember" class="md-check">
                                    <label for="remember"> <span></span> <span class="check"></span>
                                        <span class="box"></span> Remember me </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn blue"><i class="icon-login"></i> SIGN IN</button>
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>

            </div>
        </div>
    </div>
@endsection
