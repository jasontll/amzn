@extends('layouts.app')
@section('style')
    <link href="/css/blog.css" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <div class="blog-page blog-content-2">
        <div class="row">
            <div class="col-lg-9">
                <div class="blog-single-content bordered blog-container">
                    <div class="blog-single-head">
                        <h1 class="blog-single-head-title text-capitalize">{{ $article->title }}</h1>
                        <div class="blog-single-head-date">
                            <i class="icon-calendar font-blue"></i>
                            <a href="javascript:;">{{ $article->published_at->format('M d, Y') }}</a>
                        </div>
                    </div>
                    <div class="blog-single-img">
                        <img src="../assets/pages/img/background/4.jpg"></div>
                    <div class="blog-single-desc">
                        {!! $article->body !!}
                    </div>
                    @unless ($article->tags->isEmpty())
                        <div class="blog-single-foot">
                            <ul class="blog-post-tags">
                                @foreach ($article->tags as $tag)
                                    <li class="uppercase">
                                        <a href="{{url('tags/'.$tag->name)}}">{{ $tag->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endunless
                    <div class="blog-comments">
                        <h3 class="sbold blog-comments-title">Comments(30)</h3>
                        <div class="c-comment-list">
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" alt="" src="../assets/pages/img/avatars/team1.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="#">Sean</a> on <span class="c-date">23 May 2015, 10:40AM</span>
                                    </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" alt="" src="../assets/pages/img/avatars/team3.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="#">Strong Strong</a> on
                                        <span class="c-date">21 May 2015, 11:40AM</span>
                                    </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" alt="" src="../assets/pages/img/avatars/team4.jpg">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="#">Emma Stone</a> on
                                                <span class="c-date">30 May 2015, 9:40PM</span>
                                            </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" alt="" src="../assets/pages/img/avatars/team7.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="#">Nick Nilson</a> on <span class="c-date">30 May 2015, 9:40PM</span>
                                    </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                </div>
                            </div>
                        </div>
                        <h3 class="sbold blog-comments-title">Leave A Comment</h3>
                        <form action="#">
                            <div class="form-group">
                                <input type="text" placeholder="Your Name" class="form-control c-square"></div>
                            <div class="form-group">
                                <input type="text" placeholder="Your Email" class="form-control c-square"></div>
                            <div class="form-group">
                                <input type="text" placeholder="Your Website" class="form-control c-square"></div>
                            <div class="form-group">
                                <textarea rows="8" name="message" placeholder="Write comment here ..." class="form-control c-square"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn blue uppercase btn-md sbold btn-block">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                @include('articles._single-sidebar')
            </div>
        </div>
    </div>
@stop