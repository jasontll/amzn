@section('style')
    <link href="/vendor/global/plugins/medium-editor/css/themes/flat.min.css" rel="stylesheet" type="text/css"/>
@stop

@section('footer')
    {{--<script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.15/vue.js"></script>--}}
    <script>
        var bodyEditor = new MediumEditor(' .medium-editable', {
            buttonLabels: 'fontawesome',
            placeholder: false
        });
    </script>
@stop

<div id="form" class="form-body">
    <div class="form-group form-md-line-input">
        {!! Form::text('title', null, ['class'=>'form-control']) !!}
        {!! Form::label('title', 'Title:') !!}
        {{--<span class="help-block">Some help goes here...</span>--}}
    </div>
    <div class="form-group form-md-line-input">
        {{--{!! Form::textarea('body', null, ['class'=>'form-control', 'rows' => 10]) !!}--}}
        {!! Form::textarea('body', null, ['class'=>'form-control medium-editable']) !!}
        {!! Form::label('body', 'Body:') !!}
        {{--<div class="form-control-focus"></div>--}}
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group form-md-line-input">
                {!! Form::select('tag_list[]', $tags, null, ['id'=>'tag_list', 'class' => 'form-control select2-multiple select2me', 'multiple']) !!}
                {!! Form::label('tag_list', 'Tags:') !!}
            </div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="col-md-3">
            <div class="form-group form-md-line-input">
                {!! Form::input('date', 'published_at', $article->published_at->format('Y-m-d'), ['class'=>'form-control']) !!}
                {!! Form::label('published_at', 'Publish On:') !!}
            </div>
        </div>
    </div>
    <div class="form-actions noborder">
        {!! Form::submit($submitButton, ['class'=> 'btn blue']) !!}
    </div>
</div>
