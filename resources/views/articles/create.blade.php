@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-9">
            <div class="portlet light">
                <h1>New Article</h1>
                <hr>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p><strong>Error!</strong> {{$error}}</p>
                        @endforeach
                    </div>
                @endif
                <div class="portlet-body form">
                    {!! Form::model($article = new \App\Article, ['route' => 'articles.store']) !!}
                    @include('articles._form', ['submitButton' => 'Create'])
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
        <div class="col-lg-3">

        </div>
    </div>
@stop