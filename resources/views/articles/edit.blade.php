@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-9">
            <div class="portlet light">
                <h1 class="text-capitalize">Edit: {{ $article->title }}</h1>
                <hr>
                @include('errors.list')
                <div class="portlet-body form">
                    {!! Form::model($article, ['method' => 'PATCH', 'route' => ['articles.update', $article->slug]]) !!}
                    @include('articles._form', ['submitButton' => 'Update'])
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
        <div class="col-lg-3">

        </div>
    </div>
@stop
