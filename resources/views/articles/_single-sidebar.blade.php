<div class="blog-single-sidebar bordered blog-container">
    <div class="blog-single-sidebar-search">
        <div class="input-icon right">
            <i class="icon-magnifier"></i> <input type="text" class="form-control" placeholder="Search Blog"></div>
    </div>
    <div class="blog-single-sidebar-recent">
        @unless ($latestArticles->isEmpty())
            <h3 class="blog-sidebar-title uppercase">Recent Posts</h3>
            <ul>
                @foreach ($latestArticles as $article)
                    <li class="text-capitalize">
                        {{link_to_action('ArticlesController@show', $article->title, $article->slug)}}
                    </li>
                @endforeach
            </ul>
        @endunless
    </div>
    <div class="blog-single-sidebar-tags">
        @unless ($tags->isEmpty())
            <h3 class="blog-sidebar-title uppercase">Tags</h3>
            <ul class="blog-post-tags">
                @foreach ($tags as $tag)
                    <li class="uppercase">
                        {{link_to_action('TagsController@show', $tag->name, $tag->name)}}
                    </li>
                @endforeach
            </ul>
        @endunless
    </div>
    {{--<div class="blog-single-sidebar-links">--}}
    {{--<h3 class="blog-sidebar-title uppercase">Useful Links</h3>--}}
    {{--<ul>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">Lorem Ipsum </a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">Dolore Amet</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">Metronic Database</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">UI Features</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">Advanced Forms</a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="blog-single-sidebar-ui">--}}
    {{--<h3 class="blog-sidebar-title uppercase">UI Examples</h3>--}}
    {{--<div class="row ui-margin">--}}
    {{--<div class="col-xs-4 ui-padding">--}}
    {{--<a href="javascript:;">--}}
    {{--<img src="../assets/pages/img/background/1.jpg" />--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-xs-4 ui-padding">--}}
    {{--<a href="javascript:;">--}}
    {{--<img src="../assets/pages/img/background/37.jpg" />--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-xs-4 ui-padding">--}}
    {{--<a href="javascript:;">--}}
    {{--<img src="../assets/pages/img/background/57.jpg" />--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-xs-4 ui-padding">--}}
    {{--<a href="javascript:;">--}}
    {{--<img src="../assets/pages/img/background/53.jpg" />--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-xs-4 ui-padding">--}}
    {{--<a href="javascript:;">--}}
    {{--<img src="../assets/pages/img/background/59.jpg" />--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-xs-4 ui-padding">--}}
    {{--<a href="javascript:;">--}}
    {{--<img src="../assets/pages/img/background/42.jpg" />--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
</div>