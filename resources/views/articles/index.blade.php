@extends('layouts.app')
@section('style')
    <link href="/css/blog.css" rel="stylesheet" type="text/css"/>
@stop
@section('footer')
 @include('components._toastr')
@stop

@section('content')
    <div class="blog-page blog-content-1">
        <h1>Articles</h1>
        <hr>
        @foreach ($articles as $article)
            <div class="blog-post-lg bordered blog-container">
                {{--<div class="blog-img-thumb">--}}
                {{--<a href="{{ route('articles.show', [$article->slug]) }}">--}}
                {{--<img src="../assets/pages/img/page_general_search/5.jpg"> </a>--}}
                {{--</div>--}}
                <div class="blog-post-content">
                    <h2 class="blog-title blog-post-title text-capitalize">
                        <a href="{{ route('articles.show', [$article->slug]) }}">{{ $article->title }}</a>
                    </h2>
                    <p class="blog-post-desc">
                        {!! $article->body !!}
                    </p>
                    <div class="blog-post-foot">
                        @unless ($article->tags->isEmpty())
                            <ul class="blog-post-tags">
                                @foreach ($article->tags as $tag)
                                    <li class="uppercase">
                                        <a href="{{url('tags/'. $tag->name)}}">{{ $tag->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endunless
                        <div class="blog-post-meta">
                            <i class="icon-calendar font-blue"></i>
                            <a href="{{ route('articles.show', [$article->slug]) }}">{{ $article->published_at->format('M d, Y') }}</a>
                        </div>
                        <div class="blog-post-meta">
                            <i class="icon-bubble font-blue"></i> <a href="javascript:;">14 Comments</a>
                        </div>
                    </div>


                </div>
            </div>
        @endforeach
    </div>
@stop

