@extends('layouts.app')
@section('page-top')
    <div class="t-breadcrumb"><h4><i class="icon-bar-chart"></i> <a href="{{url()->current()}}">Dashboard</a></h4></div>
@endsection
@section('content')
    <h3 class="page-title"><i class="fa fa-amazon"></i> Amazon
        <small>sales performance &amp; statistics</small>
    </h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <sales-stats></sales-stats>
            </div>
        </div>
        <div class="col-md-8 col-sm-8">
            <sales-performance-chart></sales-performance-chart>
        </div>
        <div class="col-md-9 col-sm-9">
            <sales-performance-grid></sales-performance-grid>
        </div>
    </div>
@endsection

@section('page_scripts')
    <script src="{{url('/vendor/global/plugins/amcharts/amcharts.js')}}"></script>
    <script src="{{url('/vendor/global/plugins/amcharts/serial.js')}}"></script>
    <script src="{{url('/vendor/global/plugins/amcharts/themes/light.js')}}"></script>
@stop

