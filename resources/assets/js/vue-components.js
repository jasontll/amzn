var Vue = require('vue');
Vue.config.debug = true;
Vue.use(require('vue-resource'));
Vue.use(require('./directives/vue-ajax'));
Vue.use(require('./directives/vue-select'));
Vue.use(require('./directives/vue-medium-edit'));
Vue.use(require('./filters/vue-replace'));
Vue.use(require('./filters/vue-percentage'));

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="_token"]').content;

import SalesStats from "./components/SalesStats.vue";
import SalesPerformanceChart from "./components/SalesPerformanceChart.vue";
import SalesPerformanceGrid from "./components/SalesPerformanceGrid.vue";
import CategorySelect from "./components/CategorySelect.vue";
import Modal from "./components/Modal.vue";
import ActionLink from "./components/ActionLink.vue";
import MembersGrid from "./components/MembersGrid.vue";
import ProductsGrid from "./components/ProductsGrid.vue";
import ProductTypeSelect from "./components/ProductTypeSelect.vue";

new Vue({
    el: '#app',
    data: {
        invitationModal: false
    },
    components: {
        SalesStats,
        SalesPerformanceChart,
        SalesPerformanceGrid,
        CategorySelect,
        Modal,
        ActionLink,
        MembersGrid,
        ProductsGrid,
        ProductTypeSelect
    },
    events: {
        'category-changed'(data) {
            this.$broadcast('category-changed', data);
        },
        'action-triggered'(data) {
            this.$broadcast('action-triggered', data);
        }
    },
    ready(){
        console.info('Ready to go!');
    }
});