/**
 * Created by jasontll on 4/22/16.
 */
;(function () {

    var vuePercentage = {};
    // exposed global options
    vuePercentage.config = {};

    vuePercentage.install = function (Vue) {
        Vue.filter('percentage', function (value, defaultValue) {
            const digitsRE = /(\d{3})(?=\d)/g;
            defaultValue = defaultValue != null ? defaultValue : 'N/A';

            value = parseFloat(value);
            if (value < -1) return 'N/A';
            if (!isFinite(value) || (!value && value !== 0) || value < 0) return defaultValue;

            var stringified = Math.abs(value).toFixed(2);
            var _int = stringified.slice(0, -3);
            var i = _int.length % 3;
            var head = i > 0
                ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
                : '';
            var _float = stringified.slice(-3);
            var sign = value < 0 ? '-' : '';

            return sign + head +
                   _int.slice(i).replace(digitsRE, '$1,') +
                   _float + '%';
        });
    };

    if (typeof exports == "object") {
        module.exports = vuePercentage
    } else if (typeof define == "function" && define.amd) {
        define([], function () {
            return vuePercentage
        })
    } else if (window.Vue) {

        window.VuePercentage = vuePercentage;
        Vue.use(vuePercentage)
    }
})();