/**
 * Created by jasontll on 4/22/16.
 */
;(function () {

    var Replace = {};
    // exposed global options
    Replace.config = {};

    Replace.install = function (Vue) {
        Vue.filter('replace', function (value, target, replaceWith) {
            return value.replace(target, replaceWith);
        });
    };

    if (typeof exports == "object") {
        module.exports = Replace
    } else if (typeof define == "function" && define.amd) {
        define([], function () {
            return Replace
        })
    } else if (window.Vue) {
        window.Replace = Replace;
        Vue.use(Replace)
    }

})();