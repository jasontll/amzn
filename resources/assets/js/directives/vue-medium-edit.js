/**
 * Created by jasontll on 5/18/16.
 * ECMAScript Syntax
 */
;(() => {

    var vueMediumEdit = {};
    // exposed global options
    vueMediumEdit.config = {};

    vueMediumEdit.install = (Vue) => {
        Vue.directive('medium-edit', {
            bind(){
                if (!$("link[href='/vendor/global/plugins/medium-editor/css/themes/flat.min.css']").length) {
                    $('<link href="/vendor/global/plugins/medium-editor/css/themes/flat.min.css" rel="stylesheet">')
                        .appendTo("head");
                }
                $(this.el).addClass('medium-editable');
                let editor = new MediumEditor('.medium-editable', {
                    buttonLabels: 'fontawesome',
                    placeholder: false
                });
            }
        })
    };

    if (typeof exports == "object") {
        module.exports = vueMediumEdit
    } else if (typeof define == "function" && define.amd) {
        define([], () => {
            return vueMediumEdit
        })
    } else if (window.Vue) {

        window.VueMediumEdit = vueMediumEdit;
        Vue.use(vueMediumEdit)
    }
})();