;(() => {
    var vueAjax = {};
    // exposed global options
    vueAjax.config = {};

    vueAjax.install = (Vue) => {
        let defaultButtonText;
        let processButtonText;

        Vue.directive('ajax', {
            params: [
                'complete',
                'defaultButtonText',
                'processButtonText'
            ],
            bind(){
                this.el.addEventListener('submit', this.onFormSubmission.bind(this));
                defaultButtonText = this.el.querySelector('button[type="submit"]').innerHTML;
                processButtonText = 'Processing...';
                $(this.el).find('input').change(this.calmValidation);
                //this.el.querySelectorAll('input').addEventListener('change', this.calmValidation.bind(this));
            },
            onFormSubmission(e){
                this.updateButtonState(false);
                this.vm
                    .$http[this.getRequestType()](this.el.action, this.getFormData())
                    .then(this.onComplete.bind(this))
                    .catch(this.onError.bind(this));
                e.preventDefault();
            },
            onComplete(response){
                this.el.reset();
                this.updateButtonState(true);
                this.vm.$dispatch('ajax-form-success', this.el.id);
                toastr.success(response.data.message || 'Success');
                this.roundup();
            },
            onError(response) {
                this.updateButtonState(true);
                if (response.data) {
                    if (response.data.id == ('form_validation_error' || 'http_response_error')) {
                        this.handleAjaxValidation(response);
                        return;
                    }
                    toastr.error(response.data.message || 'Error');
                } else {
                    console.error(response);
                }
            },
            getRequestType(){
                var method = this.el.querySelector('input[name="_method"]');
                return (method ? method.value : this.el.method).toLowerCase();
            },
            getFormData() {
                // You can use $(this.el) in jQuery and you will get the same thing.
                var serializedData = $(this.el).serializeArray();
                return this.serializeObject(serializedData);
            },
            serializeObject(formData){
                var objectData = {};
                $.each(formData, function () {
                    if (objectData[this.name] !== undefined) {
                        if (!objectData[this.name].push) {
                            objectData[this.name] = [objectData[this.name]];
                        }
                        objectData[this.name].push(this.value || '');
                    } else {
                        objectData[this.name] = this.value || '';
                    }
                });
                return objectData;
            },
            updateButtonState(done){
                if (done) {
                    this.el.querySelector('button[type="submit"]').innerHTML = defaultButtonText;
                } else {
                    this.el.querySelector('button[type="submit"]').innerHTML = processButtonText;
                }
                this.el.querySelector('button[type="submit"]').disabled = !done;
            },
            handleAjaxValidation(response){
                let messages = [];
                let errors = response.data.detail;
                let validated = false;
                for (let validator in errors) {
                    let $parent = $('input[name="' + validator + '"]').parent();
                    if (!validated) {
                        $parent.addClass('has-error');
                    }
                    if (errors.hasOwnProperty(validator)) {
                        for (let error of errors[validator]) {
                            messages.push(error);
                        }

                        $parent.find('.help-block.help-default').addClass('hide');
                        $parent.find('.help-block:not(.help-default)').remove();
                        $parent.append('<span class="help-block">' + errors[validator][0] + '</span>');

                    }
                }
                $('.has-error:first').find('input').focus();
                toastr.warning(messages.join("<br>"))
            },
            calmValidation(){
                let $parent = $(this).parent();
                $parent.removeClass('has-error')
            },
            roundup(){
                if (this.params.complete == 'reload') {
                    location.reload();
                }
            }
        });

    };

    if (typeof exports == "object") {
        module.exports = vueAjax
    } else if (typeof define == "function" && define.amd) {
        define([], () => {
            return vueAjax
        })
    } else if (window.Vue) {
        window.VueAjax = vueAjax;
        Vue.use(vueAjax)
    }

})();