/**
 * Created by jasontll on 5/17/16.
 * ECMAScript Syntax
 */
;(() => {

    var vueSelect = {};
    // exposed global options
    vueSelect.config = {};

    vueSelect.install = (Vue) => {
        let defaultValue;
        Vue.directive('select', {
            twoWay: true,
            priority: 1000,
            params: ['options'],
            paramWatchers: {
                options: function (val, oldVal) {
                    this.initSelect2();
                    this.update();
                }
            },
            bind(){
                Vue.nextTick(()=> {
                    $.fn.select2.defaults.set("theme", "bootstrap");
                    this.initSelect2();
                })
            },
            update(value){
                defaultValue = defaultValue || value;
                $(this.el).val(defaultValue).trigger('change');
            },
            unbind(){
                $(this.el).off().select2('destroy');
            },
            initSelect2(){
                let self = this;
                if($(this.el).select2().length){
                    $(this.el).select2('destroy');
                }
                $(this.el).select2({
                    data: this.params.options
                }).on('change', ()=> {
                    self.set(this.el.value);
                })
            }
        })
    };

    if (typeof exports == "object") {
        module.exports = vueSelect
    } else if (typeof define == "function" && define.amd) {
        define([], () => {
            return vueSelect
        })
    } else if (window.Vue) {

        window.VueSelect = vueSelect;
        Vue.use(vueSelect)
    }
})();